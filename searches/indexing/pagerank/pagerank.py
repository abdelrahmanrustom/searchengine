from pymongo import MongoClient
from hashlib import sha256

# print
client = MongoClient("localhost", 27017)
db = client['crawldb']
collection = db['rawWebpages']
temp = db['temp']

d = 0.85
def build_preds():
	cursor = collection.find({'type':'html'})
	url_list = [doc['url'] for doc in cursor]
	n_docs = float(len(url_list))
	cursor = collection.find({'type':'html'})
	for doc in cursor:
		# print(cursor)
		# print(doc['url'])
		links = doc['links']
		url = doc['url']
		# print(links)
		for l in links:
			# print(l)
			if l in url_list:
				collection.update_one({"url":l}, {"$addToSet":{"preds":url}})

		# remove l
		collection.update_one({"url":url}, {"$set":{"pr":1.0}})
	return n_docs

def pr_iter():
	n = 1803.0
	sink_wt = (1.0-d)/n
	cursor = collection.find({'type':'html'})
	for doc in cursor:
		preds = doc['preds']
		res = 0.0	

		# print(preds)

		for p in preds:
			print(p)
			dc = collection.find_one({'url':p})	
			print(type(dc))
			for h in dc:
				print(h['url'])
			res += dc['pr'] / dc['l']

		res *= d

		collection.update_one({"_id":doc["_id"]}, {"$set":{"pr":res}})


def hash_urls():
	cursor = collection.find({})

	for doc in cursor:
		# _id = sha256(doc['url'].encode()).hexdigest()
		_id = doc["_id"]

		_links = []
		_title = ""
		_html = ""
		_imgs = []
		_vids = []
		if 'links' in doc:
			_links = doc['links']
			# _links = [sha256(l.encode()).hexdigest() for l in links]

		if 'html' in doc:
			_html = doc['html']

		if 'title' in doc:
			_title = doc['title']

		if 'imgs' in doc:
			_imgs = doc['imgs']

		if 'vids' in doc:
			_vids = doc['vids']

		temp.insert_one({
		"_id": _id,
		"value": {
		"url": doc['url'],
		"parentUrl": doc['parentUrl'],
		"lastModified": doc['lastModified'],
		"anchor": doc['anchor'],
		"indexed": doc['indexed'],
		"html": _html,
		"title": _title,
		"links": _links,
		"imgs": _imgs,
		"vids": _vids,
		"type": doc['type'],
		"pr": 1.0
		}})

def g():
	cursor = collection.find({"value.type":"html"})
	for doc in cursor:
		# _id = sha256(doc['url'].encode()).hexdigest()
		_id = doc["_id"]
		print(_id)
		temp.insert_one({
		"_id": _id,
		"value": {
		"links": doc["value"]["links"],
		"pr": doc["value"]["pr"]
		}})

def main():
	# n = build_preds()
	# pr_iter()
	g()





if __name__ == '__main__':
	g()