import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.BinaryParseData;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.http.Header;
import org.bson.Document;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.security.MessageDigest;


// TODO: Bulk insert
public class Crawler extends WebCrawler {
    private static final Pattern Filters = Pattern.compile(
            ".*(\\.(css|js|mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v" +
                    "|rm|smil|wmv|swf|wma|zip|rar|gz|bmp|gif|jpe?g|png|tiff?|ico|svg))$");
    private static final Pattern imgPatterns = Pattern.compile(".*(\\.(bmp|gif|jpe?g|png|tiff?))$");
    private static final Pattern vidPatterns = Pattern.compile(".*(\\.(mp4|wav|avi|mov|mpeg|wmv))$");
        private static final MongoClient mongoClient = new
                MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        private  static  final MongoDatabase database = mongoClient.getDatabase("crawldb");
        private static final MongoCollection<Document> collection = database.getCollection("rawWebpages");

    private static MessageDigest messageDigest = null;

    {
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public boolean shouldVisit(Page refferingPage, WebURL url){
        String href = url.getURL().toLowerCase();
        return !Filters.matcher(href).matches();
    }

    public void visit(final Page page){
        int docid = page.getWebURL().getDocid();
        String url = page.getWebURL().getURL();
        String domain = page.getWebURL().getDomain();
        String path = page.getWebURL().getPath();
        String subDomain = page.getWebURL().getSubDomain();
        String parentUrl = page.getWebURL().getParentUrl();
        String anchor = page.getWebURL().getAnchor();
        boolean unwantedType = false;
        Date lastModified = null;

        Document pageData = new Document();

        pageData.put("url", url);
        pageData.put("parentUrl", parentUrl);
        pageData.put("lastModified", lastModified);
        pageData.put("anchor", anchor);
        pageData.put("indexed", false);

        // TODO: Comment in production
//        logger.debug("Docid: {}", docid);
//        logger.info("URL: {}", url);
//        logger.debug("Domain: '{}'", domain);
//        logger.debug("Sub-domain: '{}'", subDomain);
//        logger.debug("Path: '{}'", path);
//        logger.debug("Parent page: {}", parentUrl);
//        logger.debug("Anchor text: {}", anchor);


        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
//            String text = htmlParseData.getText();
            String html = htmlParseData.getHtml();
            String title = htmlParseData.getTitle();
            Set<WebURL> links = htmlParseData.getOutgoingUrls();
            HashSet<String> _links = new HashSet<String>();
            HashSet<String> _imgs = new HashSet<String>();
            HashSet<String> vids = new HashSet<String>();

            for(WebURL l: links){
                if(imgPatterns.matcher(l.getURL()).matches()){
                    _imgs.add(l.getURL());
                }else if(vidPatterns.matcher(l.getURL()).matches()){
                    vids.add(l.getURL());
                }else if(!(Filters.matcher(l.getURL()).matches())){
                    _links.add(l.getURL());
                }
            }



//            pageData.put("text", text);
            pageData.put("html", html);
            pageData.put("title", title);
            pageData.put("links", _links);
            pageData.put("imgs", _imgs);
            pageData.put("vids", vids);
            pageData.put("type", "html");

//            logger.debug("Title: " + title);
//            logger.debug("Text length: {}", text.length());
//            logger.debug("Html length: {}", html.length());
//            logger.debug("Number of outgoing links: {}", links.size());


        }else if(page.getParseData() instanceof BinaryParseData){
            if(url.endsWith("pdf")){
                pageData.put("type", "pdf");
            }else if(url.contains("ppt")){
                pageData.put("type", "ppt");
            }else
                unwantedType = true;
        }else
            return;



        Header[] responseHeaders = page.getFetchResponseHeaders();
        if (responseHeaders != null) {
//            logger.debug("Response headers:");
            for (Header header : responseHeaders) {
//                logger.debug("\t{}: {}", header.getName(), header.getValue());
                if(header.getName().equals("Last-Modified")) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
                        sdf.setLenient(false);
                        lastModified = sdf.parse(header.getValue());
                        pageData.put("lastModified", lastModified);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if(!unwantedType) { // Since our patterns may not cover all extensions
            try {

                pageData.put("_id", Arrays.toString(messageDigest.digest(url.getBytes())));
                collection.insertOne(pageData);
            }catch (MongoException e){
                e.printStackTrace();
            }
        }
//        logger.debug("=============");
    }
}

