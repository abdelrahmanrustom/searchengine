from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from dmoz2csv import RDF
from csv2traintest import Dataset

from bs4 import BeautifulSoup
import requests
import string
import io
import re
from nltk.stem import WordNetLemmatizer

import unicodedata

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer

import sys
from itertools import islice

from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.ensemble import BaggingClassifier
from sklearn.svm import SVC
from sklearn import linear_model
from sklearn.metrics import classification_report
import gc
import random
from sklearn.externals import joblib
from sklearn import ensemble
from sklearn.neural_network import MLPClassifier
gbc = ensemble.GradientBoostingClassifier(verbose=3)
mlp = MLPClassifier(solver='adam', alpha=0.001, hidden_layer_sizes=[1000,], max_iter=1000, random_state=101, verbose=True,
	activation='relu')


dmoz_fname = "content.rdf.u8"
csv_fname = "content.csv"

# encoding=’utf-8’
tfidfvectorizer = TfidfVectorizer(min_df=2, max_df=0.95, strip_accents='ascii', stop_words='english', lowercase=True,
	max_features=10000)
# tfidfvectorizer = TfidfVectorizer(min_df=2, max_df=0.95, strip_accents='ascii', stop_words='english', lowercase=True)

hsh = HashingVectorizer(strip_accents='ascii', stop_words='english')
c = CountVectorizer(strip_accents='ascii', stop_words='english')
logreg = linear_model.LogisticRegression(C=1e7)
n_estimators = 10
clf = BaggingClassifier(logreg, n_jobs=-1, max_samples=1.0 / n_estimators, n_estimators=n_estimators, verbose=3)
svc = SVC(C=1, gamma=0.01)

def label2int(label):
	if label == "Non2":
		return 0
	if label == "Arts":
		return 1
	if label == "Business":
		return 2
	if label == "Computers":
		return 3
	if label == "Games":
		return 4
	if label == "Health":
		return 5
	if label == "Home":
		return 6
	if label == "News":
		return 7
	if label == "Recreation":
		return 8
	if label == "Reference":
		return 9
	if label == "Science":
		return 10
	if label == "Shopping":
		return 11
	if label == "Society":
		return 12
	if label == "Sports":
		return 13
	print("problem")


def download(url):
	try:
		request = requests.get(url, allow_redirects=True, timeout=10)
		request.raise_for_status()
	except requests.exceptions.RequestException as e:
		return None
	
	content_length = request.headers.get('content-length', None)
	if content_length and int(content_length) > 1e7:
		return None

	return request.content

def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    return True


def parse_html(doc):
	file = download(doc["url"])
	# print(file)
	if file is not None:
		html_file = BeautifulSoup(file.decode('latin-1', 'ignore'), "lxml")
		for script in html_file(["script", "style"]):
			script.decompose()    # rip it out
		data = html_file.findAll(text=True)
		text = ''.join(filter(visible, data))
		BeautifulSoup.clear(html_file)
		text = unicodedata.normalize("NFKD", text)
		text = ''.join([i if ord(i) < 128 else ' ' for i in text])
		text = text.replace("\n", " ").replace("\r", " ").replace("\t", " ")
		return text.encode("utf-8")
	else:
		return None

def rdf2csv():
	with RDF(dmoz_fname) as rdf:
		rdf.getPages()
		rdf.showTopics()
		rdf.writeCSV()
	line = "1, 2, 3" # Add this line manually at the beginning xD print


def write_text(lo, hi, fname):
	ds = pd.read_csv(fname).head(hi+1)
	X, y = ds[['2']].values, ds[['3']].values

	X = X[lo:hi+1, :]
	y = y[lo:hi+1, :]
	with open(fname.split(".")[0] + ".txt", 'a') as f:
		for idx, url in np.ndenumerate(X):
			text = parse_html({"url":url})
			if text is not None:
				f.write(url + "," + str(text) + "," + y[idx[0]][0] + "\n")

	# print(acc)
	# np.savetxt('fetched_pages.txt', acc, delimiter=" ", fmt="%s %s %s")
	# np.savetxt('garbage_pages.txt', garbage, delimiter=" ", fmt="%s")

	
def column(matrix, i):
    return [row[i] for row in matrix]

def read_txt():
	head = []
	with open("content_2_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Arts_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Business_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Computers_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Games_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Health_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Home_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_News_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Recreation_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Reference_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Science_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Shopping_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Society_train.txt") as f:
		head.extend(list(islice(f, 100)))
	with open("content_Sports_train.txt") as f:
		head.extend(list(islice(f, 100)))

	return head


def summarize(text):
	from sumy.parsers.plaintext import PlaintextParser
	from sumy.nlp.tokenizers import Tokenizer
	from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
	from sumy.nlp.stemmers import Stemmer
	from sumy.utils import get_stop_words


	LANGUAGE = "english"
	SENTENCES_COUNT = 1000000

    # or for plain text files
	parser = PlaintextParser.from_string(text, Tokenizer(LANGUAGE))
	stemmer = Stemmer(LANGUAGE)

	summarizer = Summarizer(stemmer)
	summarizer.stop_words = get_stop_words(LANGUAGE)

	return "".join([sentence._text for sentence in summarizer(parser.document, SENTENCES_COUNT)])
	

def main(fname):
	# head = read_txt()
	# _head = []
	# for l in head:
	# 	res = []
	# 	a = l.split(",")
	# 	res.append(a[0])
	# 	res.append("".join(a[1:-1]))
	# 	res.append(a[-1].strip())
	# 	_head.append(res)

	# del head
	# # print(_head[0])
	# X , y = column(_head, 1), column(_head, 2)
	# y = [label2int(l) for l in y]
	# print(y)
	# # print(len(X))
	# X = tfidfVectorizer.fit_transform(X).toarray()
	# # X_train, X_test, y_train, y_test = X[:8396], X[8396:], y[:8396], y[8396:]
	# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	# del X
	# # del y
	# # grid = GridSearchCV(SVC(), param_grid=params, cv=5, refit=True, verbose=True)
	# clf = SVC()
	
	# logreg.fit(X_train, y_train)
	# preds = logreg.predict(X_test)
	# print(classification_report(y_test, preds))

	data = []
	with open('data.txt', 'r') as f:
		data = f.readlines()

	# data = data[:524]
	print(len(data))
	dataset = []
	for l in data:
		res = []
		l = l.strip().split(",")
		res.append(l[0])
		t = "".join(l[1:-1])
		# st = summarize(t)
		res.append(t)
		# print(str(len(t) - len(st)))
		dataset.append(res)
	del data
	gc.collect()

	# dataset = [elem for elem in dataset if elem[0] != "Shopping"]

	y, X = column(dataset, 0), column(dataset, 1)
	y = [label2int(l) for l in y]
	del dataset
	gc.collect()

	X = tfidfvectorizer.fit_transform(X).toarray()
	print(len(X[0]))
	X, y = np.array(X), np.array(y)

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42, shuffle=True)

	logreg.fit(X_train, y_train)
	preds = logreg.predict(X_test)
	p = logreg.predict(X_train)

	print(classification_report(preds, y_test))
	print(classification_report(p, y_train))

	joblib.dump(logreg, 'logreg.pkl')
	joblib.dump(tfidfvectorizer, 'tfidfvectorizer.pkl')

	

if __name__ == '__main__':
	# main(sys.argv[1])
	main("")
	# print("************************************************")
	# print(parse_html({"url":"http://www.awn.com"}).decode())
