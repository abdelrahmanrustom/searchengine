from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation

from sklearn.datasets import fetch_20newsgroups
from nltk.corpus import stopwords
from nltk import word_tokenize

import gensim
from gensim import matutils
from gensim.models.ldamodel import LdaModel
from gensim.models.hdpmodel import HdpModel

from sklearn.externals import joblib
import numpy as np

n_samples = 2000
n_features = 1000
n_components = 30
n_top_words = 40


topic_words = []

def print_topics(lda, vocab, n=10):
    """ Print the top words for each topic. """
    topics = lda.show_topics(formatted=True)
    print(topics)
    # for ti, topic in enumerate(topics):
    # 	print('topic %d: %s' % (ti, ' '.join('%s/%.2f' % (t[1], t[0]) for t in topic)))

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        words = " ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        topic_words.append(words)
        message += words
    #     print(message)
    # print()

def column(matrix, i):
    return [row[i] for row in matrix]



def fit_lda(X, vocab, num_topics=5, passes=20):
    """ Fit LDA from a scipy CSR matrix (X). """
    print('fitting lda...')
    return HdpModel(matutils.Sparse2Corpus(X, documents_columns=False),	 
	id2word=dict([(i, s) for i, s in enumerate(vocab)]))


# data_samples = dataset.data[:n_samples]
data = []
with open('data.txt', 'r') as f:
	data = f.readlines()

dataset = []
for l in data:
	res = []
	l = l.strip().split(",")
	res.append(l[0])
	t = "".join(l[1:-1])
	# st = summarize(t)
	res.append(t)
	# print(str(len(t) - len(st)))
	dataset.append(res)
del data

data_samples = column(dataset, 1)


# tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2,
#                                 max_features=n_features,
#                                 stop_words='english')

tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2,
                                stop_words='english')

tf = tf_vectorizer.fit_transform(data_samples)
vocab = tf_vectorizer.get_feature_names()


# lda = fit_lda(tf, vocab)
# print_topics(lda, vocab)

lda = LatentDirichletAllocation(n_components=n_components, max_iter=5,
                                learning_method='online',
                                learning_offset=50.,
                                random_state=0)

lda.fit(tf)
tf_feature_names = tf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, n_top_words)

# print(lda.components_)

v = lda.transform(tf[0])

print(topic_words[v.argmax()])

joblib.dump(lda, "lda.pkl")
joblib.dump(tf_vectorizer, "tf_vectorizer.pkl")
joblib.dump(np.array(topic_words), "topic_words.pkl")
