var o = {}

o.map = function () {
	for (var i = 0, len = this.value.links.length; i < len; i++) {
		emit(this.value.links[i], this.value.pr/len);
	}

	emit(this['_id'], 0);
	emit(this['_id'], {
		"url": this.value.url,
		"parentUrl": this.value.parentUrl,
		"lastModified": this.value.lastModified,
		"anchor": this.value.anchor,
		"indexed": this.value.indexed, // Consider making this false to trigger the indexer
		"html": this.value.html,
		"title": this.value.title,
		"links": this.value.links,
		"imgs": this.value.imgs,
		"vids": this.value.vids,
		"type": this.value.type,
		"pr": 0.0
	});
};


o.reduce = function (key, vals) {
	var selfObj = {};
	var pr = 0.0 
	for (var i = 0; i < vals.length; i++) {
		if (vals[i] instanceof Object){
			selfObj = vals[i];
		}
		else{
			pr += vals[i];
		}
	}

	pr = (0.15) + (0.85 * pr);
	selfObj.pr = pr;
	return selfObj	
};

o.out = {out: {replace:"webpages"}, query:{"value.type":"html"}}

db.rawWebpages.mapReduce(o.map, o.reduce, o.out)








// var o = {}


// o.map = function () {
//     for (var i = 0, len = this.value.links.length; i < len; i++) {
//         emit(this.value.links[i], this.value.pr / len);
//     }
//     emit(this._id, 0);
//     emit(this._id, this.value.links);
// };


// o.reduce = function (k, vals) {
//     var links = [];
//     var pagerank = 0.0;
//     for (var i = 0, len = vals.length; i < len; i++) {
//         if (vals[i] instanceof Array)
//             links = vals[i];
//         else
//             pagerank += vals[i];
//     }
//     pagerank = 1 - 0.85 + 0.85 * pagerank;
//     return { pr: pagerank, links: links };
// };

// o.out = {out: {replace:"webpages"}}

// db.temp.mapReduce(o.map, o.reduce, o.out)