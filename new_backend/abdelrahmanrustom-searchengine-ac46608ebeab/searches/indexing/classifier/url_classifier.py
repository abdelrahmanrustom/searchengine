from dmoz2csv import RDF
from csv2traintest import Dataset

from bs4 import BeautifulSoup
import requests
import string
import io
import re
from nltk.stem import WordNetLemmatizer

import unicodedata

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.datasets import load_breast_cancer
from sklearn import linear_model
from sklearn import ensemble

from sklearn.ensemble import BaggingClassifier
from sklearn.multiclass import OneVsRestClassifier


import sys
from itertools import islice
import gc
import resource
import random
import time

def mem():
    print('Memory usage         : % 2.2f MB' % round(
        resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0/1024.0,1)
    )

dmoz_fname = "content.rdf.u8"
csv_fname = "content.csv"

countVectorizer = CountVectorizer(analyzer='char_wb')
hsh = HashingVectorizer(analyzer='char_wb')
non_alpha = re.compile('[\W_]+')
# clf = SVC()
n_estimators = 10
# clf = OneVsRestClassifier(BaggingClassifier(SVC(C=1, gamma=0.01), n_jobs=-1, max_samples=1.0 / n_estimators, n_estimators=n_estimators, verbose=3))
clf = BaggingClassifier(linear_model.LogisticRegression(C=1e5, n_jobs=-1), n_jobs=-1, max_samples=1.0 / n_estimators, n_estimators=n_estimators, verbose=3)
logreg = linear_model.LogisticRegression(C=1e5, n_jobs=-1)
gbc = ensemble.GradientBoostingClassifier(verbose=3)

def label2int(label):
	label = label.replace("Non", "")
	if label == "2":
		return 0
	if label == "Arts":
		return 1
	if label == "Business":
		return 2
	if label == "Computers":
		return 3
	if label == "Games":
		return 4
	if label == "Health":
		return 5
	if label == "Home":
		return 6
	if label == "News":
		return 7
	if label == "Recreation":
		return 8
	if label == "Reference":
		return 9
	if label == "Science":
		return 10
	if label == "Shopping":
		return 11
	if label == "Society":
		return 12
	if label == "Sports":
		return 13

def clean_url(s):
	return non_alpha.sub('', s).replace('https', '').replace('http', '').replace('www', '')

def classify():
	# ds = pd.read_csv('urls')
	# print(ds.head())
	# X, y = ds[['2']].values, ds[['3']].values

	# X = X[lo:hi+1, :]
	# y = y[lo:hi+1, :]

	head = []
	with open("content_2_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Arts_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Business_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Computers_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Games_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Health_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Home_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_News_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Recreation_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Reference_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Science_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Shopping_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Society_train.csv") as f:
		head.extend(list(islice(f, 1000)))
	with open("content_Sports_train.csv") as f:
		head.extend(list(islice(f, 1000)))


	head = np.array([l.strip().split(",") for l in head[1:]])
	X, y = head[:, 1], head[:, 2]
	# X, y = ds[['2']].values, ds[['3']].values
	X = np.array([clean_url(str(url)) for url in X])
	y = np.array([label2int(label) for label in y])
	print(set(y))
	# print(y.shape)
	# print(len(head))
	X = hsh.fit_transform(X).toarray()
	print(X[0])
	# print(countVectorizer.vocabulary_)
	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
	params = {'C':[0.1, 1, 10, 100, 1000], 'gamma':[1, 0.1, 0.01, 0.001, 1e-4]}
	# grid = GridSearchCV(SVC(), param_grid=params, cv=5, refit=True, verbose=True)
	clf.fit(X_train, y_train)
	preds = clf.predict(X_test)
	print(classification_report(y_test, preds))


def column(matrix, i):
	return [row[i] for row in matrix]


def write_text(lo, hi, fname):
	ds = pd.read_csv(fname).head(hi+1)
	X, y = ds[['2']].values, ds[['3']].values

	X = X[lo:hi+1, :]
	y = y[lo:hi+1, :]
	with open(fname.split(".")[0] + ".txt", 'a') as f:
		for idx, url in np.ndenumerate(X):
			text = parse_html({"url":url})
			if text is not None:
				f.write(url + "," + str(text) + "," + y[idx[0]][0] + "\n")


def download(url):
	try:
		request = requests.get(url, allow_redirects=True, timeout=10)
		request.raise_for_status()
	except requests.exceptions.RequestException as e:
		return None
	
	content_length = request.headers.get('content-length', None)
	if content_length and int(content_length) > 1e7:
		return None

	return request.content

def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    return True


def parse_html(doc):
	file = download(doc["url"])
	# print(file)
	if file is not None:
		html_file = BeautifulSoup(file.decode('latin-1', 'ignore'), "lxml")
		for script in html_file(["script", "style"]):
			script.decompose()    # rip it out
		data = html_file.findAll(text=True)
		text = ''.join(filter(visible, data))
		BeautifulSoup.clear(html_file)
		text = unicodedata.normalize("NFKD", text)
		text = ''.join([i if ord(i) < 128 else ' ' for i in text])
		text = text.replace("\n", " ").replace("\r", " ").replace("\t", " ")
		return text.encode("utf-8")
	else:
		return None

def get_summarized_text_from_url(url):
	from sumy.parsers.html import HtmlParser
	# from sumy.parsers.plaintext import PlaintextParser
	from sumy.nlp.tokenizers import Tokenizer
	# from sumy.summarizers.lsa import LsaSummarizer as Summarizer
	# from sumy.nlp.stemmers import Stemmer
	# from sumy.utils import get_stop_words


	try:
		LANGUAGE = "english"
		# SENTENCES_COUNT = 1000000
		try:
			parser = HtmlParser.from_url(url, Tokenizer(LANGUAGE))
		except:
			return None
	    # or for plain text files
	    # parser = PlaintextParser.from_file("document.txt", Tokenizer(LANGUAGE))
		# stemmer = Stemmer(LANGUAGE)

		return "".join([sentence._text for paragraph in parser.document._paragraphs for sentence in paragraph._sentences])
	except:
		return None
	# summarizer = Summarizer(stemmer)
	# summarizer.stop_words = get_stop_words(LANGUAGE)

	# return "".join([sentence._text for sentence in summarizer(parser.document, SENTENCES_COUNT)])

def main():
	# classify()
	ds = Dataset(csv_fname)
	X = []
	topics = ds._topics
	topics.pop("2")

	# data = np.array([[topics[label][i], label] for label in topics.keys() if label != "News" for i in range(3)])
	# X, y = column(data, 0), column(data, 1)
	
	# del ds
	# del topics
	# gc.collect()

	# combined = list(zip(X, y))
	# random.shuffle(combined)

	# X[:], y[:] = zip(*combined)
	# text = []
	# for url in X:
	# 	t = parse_html({"url":url})
	# 	if t is not None:
	# 		text.append(t)
	tt = time.time()
	text = []
	for k in topics.keys():
		for i in range(300, 400):
			topic = topics[k]
			# t = parse_html({'url':topic[i]})
			t = get_summarized_text_from_url(topic[i])
			if t is not None:
				t= t.replace("\n", " ").replace("\r", " ").replace("\t", " ")
				text.append((t, k))


	# text = np.array(text)
	# print(text.shape)
	# np.savetxt('data.txt', np.hstack((X, y, text)), delimiter=",", fmt="%s")
	# d = np.genfromtxt('data.txt', dtype='str')
	# print(d.shape)

	with open('data.txt', 'a') as f:
		for i in range(len(text)):
			f.write(text[i][1] + "," + text[i][0] + "\n")

	print(time.time() - tt)
	# del data
	# gc.collect()

	# X = [clean_url(x) for x in X]
	# y = [label2int(l) for l in y]

	# X = countVectorizer.fit_transform(X).toarray()

	# X, y = np.array(X), np.array(y)
	# # # y = np.array(y)
	# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)

	# params = {'C':[0.1, 1, 10, 100, 1000], 'gamma':[1, 0.1, 0.01, 0.001, 1e-4]}
	# # clf = RandomizedSearchCV(SVC(), param_distributions=params, cv=5, refit=True, verbose=True, n_jobs=-1)
	# gbc.fit(X_train, y_train)
	# preds = gbc.predict(X_test)
	# print(classification_report(y_test, preds))
	# print(grid.best_estimator_)


if __name__ == '__main__':
	main()
