import { combineReducers } from 'redux';
import SearchReducers from './SearchReducers';
import AuthReducer from './AuthReducer';

export default combineReducers({
	search: SearchReducers,
	auth: AuthReducer	
});