import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import { Input, Button, FormGroup,Form, FormFeedback, Label } from 'reactstrap';
class Signup extends Component {



 render() {
return (
	<div style = {{minHeight: '100vh', backgroundColor : '#fafafa'}}>
	<Form>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="firstname">First name</Label>
          <Input placeholder="First name" className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="lastname">Last name</Label>
          <Input placeholder="Last name" className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="email">Email</Label>
          <Input placeholder="Email" className="text-center " />
          <FormFeedback valid>Sweet! this email is available</FormFeedback>
          <FormFeedback invalid>Sorry! this email already exists</FormFeedback>
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="password">Password</Label>
          <Input placeholder="Password" className="text-center " />
    </FormGroup>	
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="age">Age</Label>
          <Input placeholder="Age" className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="firstname">Country</Label>
          <Input placeholder="Country" className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="profilepicture">Profile picture</Label>
          <Input placeholder="Profile picture" className="text-center " />
    </FormGroup>
        <Button  style = {style.inputStyle} onClick = {this.onButtonClicked}>Signup</Button>
        </Form>
	</div>
	);
}
}


const style = {
  
  inputStyle: {
    width: 500, marginLeft:400
}
}
export default Signup ;