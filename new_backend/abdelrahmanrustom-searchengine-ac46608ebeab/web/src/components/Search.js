import React, { Component } from 'react';
import { Form, Button, FormGroup, Label,Col, Input, Nav, NavItem, NavLink  } from 'reactstrap';
import { connect } from 'react-redux';
import { loginUser } from '../actions';
class Search extends Component {
constructor() {
    super();
    this.onButtonClicked = this.onButtonClicked.bind(this);
  }
  
 render() {

//console.log(this.props.location.state.search);
return (
<Form inline style = {{backgroundColor : '#fafafa'}}>
<FormGroup style = {{marginTop:10}}>
<img src={require("./Capture3.PNG")}  style = {style.imageStyle} width="15%"  />
        <Input  style = {style.textStyle} placeholder="Search"/>
        <img src={require("./Capture3.PNG")}  style = {style.imageStyle} className="float-right" height="10%" width="10%" />
        <Button onClick = {this.onButtonClicked}>Hello</Button>
</FormGroup>

<FormGroup >
        <Nav color="light">
          <NavLink  href="#" style = {style.navLinkStyle}>Link</NavLink> 
          <NavLink href="#" style = {style.navLinkStyle}>Link</NavLink> 
          <NavLink href="#" style = {style.navLinkStyle}>Another Link</NavLink> 
          <NavLink disabled href="#" style = {style.navLinkStyle}>Disabled Link</NavLink>
        </Nav>
        </FormGroup>
        </Form>
);
}


 onButtonClicked() {
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    this.props.loginUser({ "email":"email", "password":"password" });
  }
}

const style = {
  
  textStyle: {
    alignSelf: 'center',
    fontSize: 16,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft:5,
    paddingRight:300
  },
  imageStyle: {
    paddingTop: 10,
    paddingLeft: 15,
    backgroundColor:'#00000000',
    opacity: 0.6
  },
  navLinkStyle:{
    marginLeft:15
  }
};



const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;
  return { email, password, error, loading };
};

export default connect(mapStateToProps, { loginUser } )(Search);


// <Container>
// <Row>
// <Col  xs="6" sm="4"><img src={require("./Capture3.PNG")}  width="15%" /> </Col>
// <Col  xs="6" sm="4"><Input  placeholder="username"/> </Col>
// </Row>
// <Row>
// <Col  xs="6" sm="4"> 
// <Nav>
//           <NavItem>
//             <NavLink href="#">Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink href="#">Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink href="#">Another Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink disabled href="#">Disabled Link</NavLink>
//           </NavItem>
//         </Nav>
// </Col>

// </Row>
// </Container>
