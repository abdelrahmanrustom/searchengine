import React, { Component } from 'react';
import { Link } from 'react-router-dom'

import { Input, Button, FormGroup,Form, FormFeedback, Label } from 'reactstrap';
class Signin extends Component {



 render() {
return (
	<div style = {{minHeight: '100vh', backgroundColor : '#fafafa'}}>
	<Form>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="email">Email</Label>
          <Input placeholder="Email" className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="password">Password</Label>
          <Input placeholder="Password" className="text-center " />
    </FormGroup>
        <Button  style = {style.inputStyle} onClick = {this.onButtonClicked}>Sign in</Button>
        <Link  to = {{ pathname: '/signup'}} ><a style = {{marginLeft: 10}} href="#">Sign up</a> </Link>
          
        </Form>
	</div>
	);
}
}


const style = {
  
  inputStyle: {
    width: 500, marginLeft:400
}
}
export default Signin ;