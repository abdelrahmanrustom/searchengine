import React, { Component } from 'react';
import './css/home.css'
//import { matchPath } from 'react-router'
import { Link } from 'react-router-dom'

class Home extends Component {

// const Home = () => {

constructor(props){
   super(props);

   //this.state={inputfield: "no value"};   

    this.state = { search_text: "" };
 // this.doodleSearchClick = this.doodleSearchClick.bind(this);
   this.searchText = this.searchText.bind(this);
   //TODO i am feeling lucky
  }

searchText(evt) {
   
    this.setState({search_text: evt.target.value});
    //this.props.search = evt.target.value;
  }


// doodleSearchClick(){
//   // this.props.router.push('')
//    console.log(this.state.search_text); 
// }  


  render() {
return (
  <body>
    <header>
      <nav>
        <ul id="nav_bar">
          <li class="nav-links"><a href="#">Images</a></li>
          <Link to = {{ pathname: '/signin'}} > <li class="nav-links"><a href="#">Sign In</a></li> </Link>
          
        </ul>  
      </nav>  
    </header>  
    
    <div class="google">
      <a href="#" id="google_logo"><img src={require("./Capture3.PNG")}/></a>
    </div>
    
    <div class="form">  
      <form>
        <label for="form-search"></label>
        <input type="text" id="form-search" placeholder="Search doodle or type URL" onChange={this.searchText} />
      </form>
    </div>  
    
    <div class= "buttons">  
    <Link to = {{ pathname: '/search', state: { search: this.state.search_text} }} ><input type="submit" value="Doodle Search"/></Link>
      
      <input type="submit" value="I'm Feeling Lucky" id="im_feeling_lucky"/>
    </div>
    <footer>
        <ul class="footer-left">
          <li><a href="#">Advertising</a></li>
          <li><a href="#">Business</a></li>
          <li><a href="#">About</a></li> 
        </ul>
        <ul class="footer-right">    
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Terms</a></li>
          <li><a href="#">Settings</a></li>
        </ul>       
    </footer>      
  </body>
	);
}
}


export default Home ;