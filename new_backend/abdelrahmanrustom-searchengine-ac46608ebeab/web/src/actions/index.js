import {
  loginUserCall
} from './AuthActions';

export const loginUser = ({ email, password }) => {
	return loginUserCall({ email, password });
};
