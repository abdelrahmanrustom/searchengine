import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './components/home.js';
import Search from './components/Search.js';
import Signup from './components/Signup.js';
import Signin from './components/Signin.js';

class RouterComponent extends Component {
	render() {
	return (
		<Router>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/search" component={Search} />
				<Route path="/signup" component={Signup} />
				<Route path="/signin" component={Signin} />
			</Switch>
		</Router>
	);
	}
}

export default RouterComponent ;