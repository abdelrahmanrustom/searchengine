package loadbalancer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class LoadBalancerClient {
    private ArrayList<String> clientArrayList;
    private ArrayList<String> replyArrayList;
    private ArrayList<Channel> channels;

    private final Map<String, Consumer<byte[]>> _continuationMap = new HashMap<String, Consumer<byte[]>>();
    int size;
    int counter = 0;

    public LoadBalancerClient(int size) throws IOException, TimeoutException {
        clientArrayList = new ArrayList<>();
        replyArrayList = new ArrayList<>();
        channels = new ArrayList<>();
        this.size = size;
        for (int i = 1; i <= size; i++) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("rabbitmq");

            Connection connection;
            Channel channel;

            connection = factory.newConnection();
            channel = connection.createChannel();
            System.out.println("rabbitmq-" + i);

            channel.queueDeclare("rabbitmq-" + i, false, false, true, null);
            // channel.basicQos(10);
            clientArrayList.add("rabbitmq-" + i);
            String replyQueue = channel.queueDeclare(
                    "Reply-To-LoadBalancer-From-" + i,
                    false,
                    false,
                    true,
                    null).getQueue();
            replyArrayList.add(replyQueue);
            channels.add(channel);

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    channel.queueDelete(replyQueue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));

            channel.basicConsume(replyQueue, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    Consumer<byte[]> consumer;
                    synchronized (_continuationMap) {
                        consumer = _continuationMap.remove(properties.getCorrelationId());
                    }
                    if(consumer != null) {
                        consumer.accept(body);
                        System.out.println("client receving");
                    }
                }
            });
        }
    }

    public void call(byte[] message, Consumer<byte[]> callBack) {
        try {
            AMQP.BasicProperties props;
            String client;
            Channel ch;
            synchronized (_continuationMap) {
                client = clientArrayList.get(counter);
                ch = channels.get(counter);
                final String corrId = UUID.randomUUID().toString();

                props = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(corrId)
                        .replyTo(replyArrayList.get(counter))
                        .build();
                String replyId = "" + corrId;
                _continuationMap.put(replyId, callBack);
                counter = (counter + 1) % size;
            }
            ch.basicPublish("", client, props, message);
            System.out.println(new String(message));
            System.out.println("publish to: " + client);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
