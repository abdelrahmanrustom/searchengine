package http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

public class HTTPServer {
    public static void main(String[] args) throws InterruptedException {
        EventLoopGroup bossEventLoopGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerEventLoopGroup = new NioEventLoopGroup(4);
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.localAddress(new InetSocketAddress("127.0.0.1", 8080));
            bootstrap.group(bossEventLoopGroup, workerEventLoopGroup);
            bootstrap.channel(NioServerSocketChannel.class);
            bootstrap.handler(new LoggingHandler(LogLevel.INFO));
            bootstrap.childHandler(new HTTPServerPipelineInitializer());
            bootstrap.bind().sync().channel().closeFuture().sync();
        } finally {
            bossEventLoopGroup.shutdownGracefully().sync();
            workerEventLoopGroup.shutdownGracefully().sync();
        }
    }
}
