package http;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;

import java.util.List;

public class JsonRequestDecoder extends MessageToMessageDecoder<FullHttpRequest> {
    @Override
    protected void decode(ChannelHandlerContext ctx, FullHttpRequest msg, List<Object> out) {
        ByteBuf requestBody = msg.content();
        String requestBodyString = requestBody.toString(CharsetUtil.UTF_8);
        JSONObject jsonRequest = new JSONObject(requestBodyString);
        out.add(jsonRequest);
    }
}
