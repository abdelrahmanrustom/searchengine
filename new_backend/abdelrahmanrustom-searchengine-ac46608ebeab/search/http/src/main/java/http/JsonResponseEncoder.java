package http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;

import java.util.List;

public class JsonResponseEncoder extends MessageToMessageEncoder<Object> {
    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, List<Object> out) {
        if (msg instanceof JSONObject) {
            JSONObject responseJsonObject = (JSONObject) msg;
            byte[] responseBodyContent = responseJsonObject.toString().getBytes(CharsetUtil.UTF_8);
            ByteBuf responseBody = Unpooled.copiedBuffer(responseBodyContent);
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
                    HttpResponseStatus.OK, responseBody);
            out.add(response);
        }
    }
}
