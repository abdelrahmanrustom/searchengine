package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class SetMaxDBConnectionCountCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public SetMaxDBConnectionCountCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.setMaxDBConnectionCount(Integer.parseInt(params.getString("count")));
        return new JSONObject().put("Success", "true");
    }
}
