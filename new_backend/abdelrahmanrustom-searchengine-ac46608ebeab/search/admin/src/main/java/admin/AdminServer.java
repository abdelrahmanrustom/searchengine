package admin;

import core.common.*;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class AdminServer {
    private static final Logger logger = Logger.getLogger(AdminServer.class.getName());

    private Server server;

    private RpcServer rpcServer;
    private Properties properties;

    private ClassManager classManager;
    private DependencyManager dependencyManager;

    public AdminServer(RpcServer rpcServer, Properties properties) {
        this.rpcServer = rpcServer;
        this.properties = properties;

        Properties classMap = new Properties();
        try {
            InputStream is = getClass().getResourceAsStream("classmap.properties");
            classMap.load(is);
        } catch (IOException exception) {
            exception.printStackTrace();
            System.exit(0);
        }

        this.classManager = new ClassManager(classMap);
        this.dependencyManager = new DependencyManager();
        dependencyManager.add(this.rpcServer);
    }

    public void start() throws IOException {
        /* The port on which the server should run */
        int port = 50051;
        server = ServerBuilder.forPort(port)
                .addService(new AdminImpl(classManager, dependencyManager))
                .build()
                .start();
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            AdminServer.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    public void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    static class AdminImpl extends AdminGrpc.AdminImplBase {

        private ClassManager classManager;
        private DependencyManager dependencyManager;

        public AdminImpl(ClassManager classManager, DependencyManager dependencyManager) {
            this.classManager = classManager;
            this.dependencyManager = dependencyManager;
        }

        private AdminReply processRequest(AdminRequest req) {
            JSONObject request = new JSONObject();
            req.getParamsMap().forEach(request::put);
            request.put("ServiceCommand", req.getName());
            JSONObject jsonReply = new JSONObject();
            try {
                Command cmd = CommandParser.parseCommand(classManager, dependencyManager, request);
                jsonReply = cmd.call();
            } catch (Exception e) {
                e.printStackTrace();
                jsonReply.put("Error", e.toString());
            }
            AdminReply reply = AdminReply
                    .newBuilder()
                    .setMessage("Hello " + req.getName())
                    .putAllParams((Map) jsonReply.toMap())
                    .build();
            return reply;
        }

        @Override
        public void performRequest(AdminRequest req, StreamObserver<AdminReply> responseObserver) {
            AdminReply reply = processRequest(req);
            responseObserver.onNext(reply);
            responseObserver.onCompleted();
        }
    }
}
