package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class FreezeCommand extends Command {
    private Configurable receiver;

    public FreezeCommand(Configurable server) {
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.freeze();
        return new JSONObject().put("Success", "true");
    }
}
