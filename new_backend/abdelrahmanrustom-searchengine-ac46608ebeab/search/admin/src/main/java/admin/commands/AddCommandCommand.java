package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class AddCommandCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public AddCommandCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.addCommand(params.getString("command name"));
        return new JSONObject().put("Success", "true");
    }
}
