package admin;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminClient {
    private static final Logger logger = Logger.getLogger(AdminClient.class.getName());

    private final ManagedChannel channel;
    private final AdminGrpc.AdminBlockingStub blockingStub;

    /** Construct client connecting to HelloWorld server at {@code host:port}. */
    public AdminClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build());
    }

    /** Construct client for accessing RouteGuide server using the existing channel. */
    AdminClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = AdminGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /** Say hello to server. */
    public void performRequest(String name, Map<String, String> params) {
        logger.info("Will try to greet " + name + " ...");
        AdminRequest request = AdminRequest.newBuilder().setName(name).putAllParams(params).build();
        AdminReply response;
        try {
            response = blockingStub.performRequest(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "users failed: {0}", e.getStatus());
            return;
        }
        logger.info("Greeting: " + response.getMessage() + ", " + response.getParamsMap());
    }

    public static void main(String[] args) throws Exception {
        AdminClient client = new AdminClient("localhost", 50051);
        try {
            /* Access a service running on the local machine on port 50051 */
            String user = "UnfreezeCommand";
            Map<String, String> params = new HashMap<>();
            // params.put("command name", "admin.commands.UnfreezeCommand");
            if (args.length > 0) {
                user = args[0]; /* Use the arg as the name to greet if provided */
            }
            client.performRequest(user, params);
        } finally {
            client.shutdown();
        }
    }
}
