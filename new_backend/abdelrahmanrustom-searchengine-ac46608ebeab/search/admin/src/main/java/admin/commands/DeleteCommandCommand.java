package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class DeleteCommandCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public DeleteCommandCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.deleteCommand(params.getString("command name"));
        return new JSONObject().put("Success", "true");
    }
}
