package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class SetMaxThreadPoolCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public SetMaxThreadPoolCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.setMaxThreadCount(Integer.parseInt(params.getString("count")));
        return new JSONObject().put("Success", "true");
    }
}
