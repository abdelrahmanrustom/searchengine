package admin.commands;

import admin.Configurable;
import core.common.Command;
import org.json.JSONObject;

public class UnfreezeCommand extends Command {
    private Configurable receiver;

    public UnfreezeCommand(Configurable server) {
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.unfreeze();
        return new JSONObject().put("Success", "true");
    }
}
