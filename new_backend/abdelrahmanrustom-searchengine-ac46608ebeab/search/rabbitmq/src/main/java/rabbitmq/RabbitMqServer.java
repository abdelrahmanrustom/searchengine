package rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class RabbitMqServer {
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;

    private ConsumerWorker consumer;

    private String host;
    private static  String RPC_QUEUE_NAME ;

    RabbitMqClient rabbitMqClient;

    public RabbitMqServer(String queueName, String[] apps) throws IOException, TimeoutException {
        this.host = queueName;
        initConnectionFactory();
        RPC_QUEUE_NAME = queueName + "-1";
        rabbitMqClient = new RabbitMqClient(queueName, apps);
        start();
    }

    public static void main(String... args) throws Exception {
        RabbitMqServer server = new RabbitMqServer(args[0], Arrays.copyOfRange(args, 1, args.length));
    }


    private void initConnectionFactory() {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
    }




    public void start() throws IOException, TimeoutException {
        connection = connectionFactory.newConnection(Executors.newSingleThreadExecutor((runnable) ->
                new Thread(Thread.currentThread().getThreadGroup(), runnable,"Consumer Thread")));


        channel = connection.createChannel();

        channel.queueDeclare(RPC_QUEUE_NAME, false, false, true, null);
        // channel.basicQos(10);

        consumer = new ConsumerWorker();

        consumer.start();

    }



    public void shutdown() throws IOException, InterruptedException, TimeoutException {
        if (consumer != null) {
            consumer.shutdown();
        }
        if (channel != null && channel.isOpen()) {
            channel.close();
        }
        if (connection != null && connection.isOpen()) {
            connection.close();
        }
    }

    private final class ConsumerWorker {
        private DefaultConsumer consumer = new CommandRpcConsumer(channel);

        public void start() {
            try {
                // System.out.println(Thread.currentThread());
                channel.basicConsume(RPC_QUEUE_NAME, true, consumer);
            } catch (IOException exception) {
                exception.printStackTrace();
                try {
                    this.shutdown();
                } catch (Exception shutdownException) {
                    shutdownException.printStackTrace();
                }
            }
        }

        void shutdown() {
            try {
                channel.basicCancel(consumer.getConsumerTag());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    private final class CommandRpcConsumer extends DefaultConsumer {
        CommandRpcConsumer(Channel channel) {
            super(channel);
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            rabbitMqClient.call(body, (byte[] replyBody) -> {
                String correlationId = properties.getCorrelationId();
                String replyTo = properties.getReplyTo();
                AMQP.BasicProperties.Builder replyPropertiesBuilder
                        = new AMQP.BasicProperties.Builder().correlationId(correlationId);
                AMQP.BasicProperties replyProperties = replyPropertiesBuilder.build();
                try {
                    System.out.println("Server sending");
                    channel.basicPublish("", replyTo, replyProperties, replyBody);
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            });

        }
    }

}
