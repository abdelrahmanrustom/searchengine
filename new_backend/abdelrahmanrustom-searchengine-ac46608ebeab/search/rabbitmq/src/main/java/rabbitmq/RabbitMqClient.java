package rabbitmq;

import com.rabbitmq.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class RabbitMqClient {
    private Connection connection;
    private Channel channel;
    private int appSize;

    private Map<String, String> replyQueueName = new HashMap<>();

    private final Map<String, Consumer<byte[]>> _continuationMap = new HashMap<String, Consumer<byte[]>>();
    private ArrayList<String> applications;

    public RabbitMqClient(String rmqid, String[] apps) throws IOException, TimeoutException {
        applications = new ArrayList<>(Arrays.asList(apps));
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rmqid);

        connection = factory.newConnection();
        channel = connection.createChannel();

        appSize = applications.size();

        for (int i = 0; i < appSize ; i++){
            channel.queueDeclare(applications.get(i) + "", false, false, true, null);
            // channel.basicQos(10);

            String replyQueue = channel.queueDeclare(
                    "Reply-To-RabbitMQ-" + rmqid + "-" + applications.get(i),
                    false,
                    false,
                    true,
                    null
            ).getQueue();

            replyQueueName.put(applications.get(i) + "", replyQueue);

            channel.basicConsume(replyQueue, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    Consumer<byte[]> consumer;
                    synchronized (_continuationMap) {
                        consumer = _continuationMap.remove(properties.getCorrelationId());
                    }
                    if(consumer != null) {
                        consumer.accept(body);
                        System.out.println("client receving");
                    }
                }
            });
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                for(String queue : replyQueueName.keySet()) {
                    channel.queueDelete(queue);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }

    public void call(byte[] message, Consumer<byte[]> callBack) {
        JSONObject jsonObject = new JSONObject(new String(message));
        String appName = jsonObject.getString("destination service");
        if(!applications.contains(appName)) {
            JSONObject res = new JSONObject()
                    .put("response", "")
                    .put("error", "Bad Request")
                    .put("status", "false");
            callBack.accept(res.toString().getBytes());
            return;
        }

        final String corrId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName.get(appName))
                .build();

        try {
            synchronized (_continuationMap) {
                String replyId = "" + corrId;
                _continuationMap.put(replyId, callBack);
            }
            channel.basicPublish("", appName, props, message);
            System.out.println(new String(message));
            System.out.println("publish to: " + appName);
            System.out.println(new String(message));
        } catch (Exception exception) {
            synchronized (_continuationMap) {
                String replyId = "" + corrId;
                _continuationMap.remove(replyId);
            }
            exception.printStackTrace();
            JSONObject res = new JSONObject()
                    .put("response", "")
                    .put("error", exception.toString())
                    .put("status", "false");
            callBack.accept(res.toString().getBytes());
        }
    }
}
