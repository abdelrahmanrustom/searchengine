package searchapp.app;

import admin.Configurable;
import core.common.RpcServer;
import core.dependencies.SQLDBConnectionPool;

import java.util.Properties;

public class SearchRpcServer extends RpcServer implements Configurable {
    public SearchRpcServer(Properties properties, String name) {
        super(properties, name);
    }

    @Override
    public void setMaxThreadCount(int maxThreadCount) {
        if(maxThreadCount < executorService.getCorePoolSize()) {
            executorService.setCorePoolSize(maxThreadCount);
            executorService.setMaximumPoolSize(maxThreadCount);
        } else {
            executorService.setMaximumPoolSize(maxThreadCount);
            executorService.setCorePoolSize(maxThreadCount);
        }
    }

    @Override
    public void setMaxDBConnectionCount(int maxDBConnectionCount) {
        try {
            ((SQLDBConnectionPool) dependencyManager.get(SQLDBConnectionPool.class))
                    .setMaxDBConnectionCount(maxDBConnectionCount);
        } catch (Exception exception) {
            exception.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void addCommand(String commandName) {
        updateClass(commandName);
    }

    @Override
    public void deleteCommand(String commandName) {
        classManager.deleteClass(commandName);
    }

    @Override
    public void updateCommand(String commandName) {
        updateClass(commandName);
    }

    @Override
    public void updateClass(String className) {
        classManager.updateClass(className);
    }

    @Override
    public void freeze() {
        publisher.freeze();
        consumer.freeze();
        executorService.shutdown();
        executorService = null;
        executorCompletionService = null;
        try {
            channel.close();
            connection.close();
            channel = null;
            connection = null;
            publisherThread = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unfreeze() {
        initExecutorCompletionService();
        initConnectionFactory();
        try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setErrorReportingLevel(int errorReportingLevell) {
        errorReportingLevel = errorReportingLevell;
    }
}
