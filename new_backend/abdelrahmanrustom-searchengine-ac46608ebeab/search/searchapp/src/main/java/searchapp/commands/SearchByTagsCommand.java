package searchapp.commands;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import core.common.Command;
import org.apache.http.HttpHost;
import org.bson.Document;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONObject;
import org.tartarus.snowball.ext.PorterStemmer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//https://github.com/harryaskham/Twitter-L-LDA/blob/master/util/Stopwords.java

public class SearchByTagsCommand extends Command {
    private JSONObject params;
    String q = "";
    int resultType = 0, page_no = 1, category = -1;

    final RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http"),
                    new HttpHost("localhost", 9201, "http")));

    PorterStemmer porterStemmer = new PorterStemmer();
    private static final MongoClient mongoClient = new
            MongoClient(new MongoClientURI("mongodb://localhost:27017"));
    private  static  final MongoDatabase database = mongoClient.getDatabase("history");
    private static final MongoCollection<Document> collection = database.getCollection("user_history");

    private static String[] stopwords = {"a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards", "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent", "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly", "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont", "done", "down", "downwards", "during", "each", "edu", "eg", "eight", "either", "else", "elsewhere", "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "far", "few", "ff", "fifth", "first", "five", "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having", "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i", "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate", "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll", "its", "its", "itself", "just", "keep", "keeps", "kept", "know", "knows", "known", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "little", "look", "looking", "looks", "ltd", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile", "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "ts", "take", "taken", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon", "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "un", "under", "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses", "using", "usually", "value", "various", "very", "via", "viz", "vs", "want", "wants", "was", "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what", "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole", "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would", "would", "wouldnt", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours", "yourself", "yourselves", "zero"};
    private static Set<String> stopWordSet = new HashSet<String>(Arrays.asList(stopwords));

    static Integer devReportLvl = 1; // TODO: Get from App.
    private String keywords = "";

    private SearchByTagsCommand(int category, String q){
        this.category = category;
        this.q = q;
    }

    public SearchByTagsCommand(JSONObject params){
        this.params = params;
    }

    public void parseParams(String q, int resultType, int page_no, int category, String keywords){
        this.q = q;
        this.resultType = resultType;
        this.page_no = page_no;
        this.category = category;
        this.keywords = keywords;
    }

    private static boolean isStopword(String word) {
        if(word.length() < 2) return true;
        if(word.charAt(0) >= '0' && word.charAt(0) <= '9') return true; //remove numbers, "25th", etc
        if(stopWordSet.contains(word)) return true;
        else return false;
    }

    private String removeStopWordsAndStem(String[] string) {
        String result = "";
        for(String word : string) {
            if(word.isEmpty()) continue;
            if(isStopword(word)) continue; //remove stopwords
            porterStemmer.setCurrent(word);
            porterStemmer.stem();
            result += (porterStemmer.getCurrent()+" ");

        }
        return result;
    }

    public JSONObject call(){

        try {
            parseParams(params.getString("q"), params.getInt("resultType"),
                    params.getInt("page_no"), params.getInt("category"),
                    params.getString("keywords"));
        }catch (Exception e){
            JSONObject res = new JSONObject()
                    .put("response", "")
                    .put("error", "Bad request")
                    .put("status", "false");
            return res;
        }

        String[] words = q.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");;

        if(words.length > 1)
            q = removeStopWordsAndStem(words);

        SearchRequest searchRequest = new SearchRequest("webpages");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(10*page_no);
        searchSourceBuilder.from(10*page_no - 1);
        BoolQueryBuilder tmp = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("plain_text",
                        q))
                .must(QueryBuilders.matchQuery("category",
                        category))
                .should(QueryBuilders.matchQuery("keywords", keywords).fuzziness(Fuzziness.AUTO));

        if(resultType == 1)
            tmp = tmp.filter(QueryBuilders.termQuery("has_imgs", true));
        if(resultType == 2)
            tmp = tmp.filter(QueryBuilders.termQuery("has_vids", true));


        FunctionScoreQueryBuilder query = QueryBuilders.functionScoreQuery(tmp,
                ScoreFunctionBuilders.fieldValueFactorFunction("pagerank"))
                .boostMode(CombineFunction.SUM);
        searchSourceBuilder.query(query);
        searchRequest.source(searchSourceBuilder);

        if(page_no > 20){
            JSONObject response = new JSONObject()
                    .put("response", "")
                    .put("error", "Maximum number of pages exceeded!")
                    .put("status", "false");
            return response;
        }

        try {
            SearchHits hits =  client.search(searchRequest).getHits();
            ArrayList<String> json_search_responses = new ArrayList<>();
            for(SearchHit hit: hits){
                json_search_responses.add(hit.getSourceAsString());
            }
//                System.out.println(json_search_responses);
            JSONObject response = new JSONObject()
                    .put("response", json_search_responses)
                    .put("error", "")
                    .put("status", "true");
            return response;
        } catch (IOException e) {
            String err = devReportLvl == 1 ? e.toString() : "Server failure!";
            JSONObject response = new JSONObject()
                    .put("response", "")
                    .put("error", err)
                    .put("status", "false");
            return  response;
        }
    }
}
