package searchapp.commands;

import core.common.Command;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCommand extends Command {
    private JSONObject params;
    private String q;

    final RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http"),
                    new HttpHost("localhost", 9201, "http")));

    private Integer devReportLvl;

    public void parseParams(String q){
        this.q = q;
    }

    public AutoCompleteCommand(JSONObject params){
        this.params = params;
    }

    private AutoCompleteCommand(String q){
        this.q = q;
    }

    @Override
    public JSONObject call() throws Exception {
        try {
            parseParams(params.getString("q"));
        }catch (Exception e){
            JSONObject res = new JSONObject()
                    .put("response", "")
                    .put("error", "Bad request")
                    .put("status", "false");
            return res;
        }


        try {
            Suggest hits =  client.search(new SearchRequest().source(
                    new SearchSourceBuilder().suggest(
                            new SuggestBuilder().addSuggestion(
                                    "autocomplete",
                                    SuggestBuilders.completionSuggestion("suggest")
                            .prefix(q, Fuzziness.AUTO).skipDuplicates(true).size(5))
                    ))).getSuggest();


            ArrayList<List> json_search_responses = new ArrayList<>();
            for(Suggest.Suggestion hit: hits){
                json_search_responses.add(hit.getEntries());
            }
//                System.out.println(json_search_responses);
            JSONObject response = new JSONObject()
                    .put("response", json_search_responses)
                    .put("error", "")
                    .put("status", "true");
            return response;
        } catch (IOException e) {
            String err = devReportLvl == 1 ? e.toString() : "Server failure!";
            JSONObject response = new JSONObject()
                    .put("response", "")
                    .put("error", err)
                    .put("status", "false");
            return  response;
        }
    }
}
