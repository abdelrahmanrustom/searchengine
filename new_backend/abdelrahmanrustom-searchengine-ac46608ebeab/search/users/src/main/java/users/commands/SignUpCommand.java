package users.commands;

import core.common.Command;
import core.dependencies.ReddisCluster;
import core.dependencies.SQLDBConnectionPool;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;

public class SignUpCommand extends Command {

    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;

    public SignUpCommand(SQLDBConnectionPool pool, ReddisCluster reddisCluster, JSONObject params) {
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = reddisCluster;
    }

    @Override
    public JSONObject call() {
        JSONObject response;
        try {
            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString("signup");
                        tmp2 = tmp2.substring(1, tmp2.length() - 1);
                        String[] tmps = tmp2.split(",");
                        tmp.put("id", tmps[0]);
                        tmp.put("access_token", tmps[1]);
                        tmp.put("first_name", this.params.getString("first_name"));
                        tmp.put("email", this.params.getString("email"));
                        tmp.put("last_name", this.params.getString("last_name"));
                        tmp.put("pass", this.params.getString("pass"));
                        tmp.put("age", this.params.getString("age"));
                        tmp.put("country", this.params.getString("country"));
                        tmp.put("profile_pic", this.params.getString("profile_pic"));

                        reddisCluster.set(tmps[0], tmp.toString());
                        return new JSONObject()
                                .put("response", tmp)
                                .put("error", "")
                                .put("status", "true");
                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "Something went wrong!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.getClass())
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.toString())
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            response = new JSONObject()
                    .put("response", "")
                    .put("error", e.getClass())
                    .put("status", "false");
        }
        return response;
    }

    private String getQueryString() {
        MessageFormat mf = new MessageFormat("SELECT signup(''{0}'', ''{1}'', ''{2}'', ''{3}'', {4}, {5}, ''{" +
                "6}'');");
        Object[] parsedParams = new Object[]{
                params.get("first_name"),
                params.get("last_name"),
                params.get("email"),
                params.get("pass"),
                params.get("age"),
                params.get("country"),
                params.get("profile_pic")};

        return mf.format(parsedParams);
    }
}