package users.commands;

import core.common.Command;
import core.dependencies.ReddisCluster;
import core.dependencies.SQLDBConnectionPool;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;

public class UpdateUserCommand extends Command {

    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;
    private String coloumnResult;

    public UpdateUserCommand(SQLDBConnectionPool pool, ReddisCluster reddisCluster, JSONObject params) {
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = reddisCluster;
    }

    @Override
    public JSONObject call() {
        JSONObject response;
        try {
            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString(coloumnResult);

                        tmp2 = tmp2.substring(1, tmp2.length()-1);
                        String[] tmps = tmp2.split(",");
                        if (coloumnResult.equals("update_user_email")){
                            String first = tmps[0];
                            return new JSONObject()
                                    .put("response", "")
                                    .put("error", " email already exists")
                                    .put("status", "false");
                        }
                        tmp.put("id", tmps[0]);
                        tmp.put("first_name", tmps[1]);
                        tmp.put("last_name", tmps[2]);
                        tmp.put("email", tmps[3]);
                        tmp.put("pass", tmps[4]);
                        tmp.put("age", tmps[5]);
                        tmp.put("country", tmps[6]);
                        tmp.put("profile_pic", tmps[7]);
                        tmp.put("access_token", tmps[8]);
//                        String users  = reddisCluster.get(tmps[0]);
//                        if (users != null) {
//                            reddisCluster.delete(tmps[0]);
//                        }
                        reddisCluster.set(tmps[0], tmp.toString());
                        return new JSONObject()
                                .put("response", tmp)
                                .put("error", "")
                                .put("status", "true");
                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "Something went wrong!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.getClass())
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.getClass())
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            if (e.getLocalizedMessage().contains("duplicate key value violates unique constraint")){
                response =  new JSONObject()
                        .put("response", "")
                        .put("error", "email already exists")
                        .put("status", "false");
            } else {
                response =  new JSONObject()
                        .put("response", "")
                        .put("error", e.getLocalizedMessage())
                        .put("status", "false");}
        }
        return response;
    }

    private String getQueryString() {
        MessageFormat mf;
//                = new MessageFormat
//                ("SELECT signup(''{0}''," +
//                        " ''{1}''," +
//                        " ''{2}''," +
//                        " ''{3}''," +
//                        " {4}," +
//                        " {5}, ''{" + "6}'');");
        Object[] parsedParams;

        if (params.has("first_name")){
            parsedParams = new Object[] {
                    params.get("id"), params.get("access_token"),
                    params.getString("first_name")};
            mf = new MessageFormat("SELECT update_user_firstname({0}, ''{1}'',''{2}'');");
            coloumnResult = "update_user_firstname";
            return mf.format(parsedParams);
        } else {
            if (params.has("last_name")) {
                parsedParams = new Object[] {
                        params.get("id"), params.get("access_token"),
                        params.getString("last_name")};
                mf = new MessageFormat("SELECT update_user_lastname({0}, ''{1}'',''{2}'');");
                coloumnResult = "update_user_lastname";
                return mf.format(parsedParams);
            } else {
                if (params.has("email")) {
                    parsedParams = new Object[] {
                            params.get("id"), params.get("access_token"),
                            params.getString("email")};
                    mf = new MessageFormat("SELECT update_user_email({0}, ''{1}'',''{2}'');");
                    coloumnResult = "update_user_email";
                    return mf.format(parsedParams);
                } else {
                    if (params.has("pass")) {
                        parsedParams = new Object[] {
                                params.get("id"), params.get("access_token"),
                                params.getString("pass")};
                        mf = new MessageFormat("SELECT update_user_pass({0}, ''{1}'',''{2}'');");
                        coloumnResult = "update_user_pass";
                        return mf.format(parsedParams);
                    } else {
                        if (params.has("age")) {
                            parsedParams = new Object[] {
                                    params.get("id"), params.get("access_token"),
                                    params.getInt("age")};
                            mf = new MessageFormat("SELECT update_user_age({0}, ''{1}'',{2});");
                            coloumnResult = "update_user_age";
                            return mf.format(parsedParams);
                        } else {
                            if (params.has("country")) {
                                parsedParams = new Object[] {
                                        params.get("id"), params.get("access_token"),
                                        params.getInt("country")};
                                mf = new MessageFormat("SELECT update_user_country({0}, ''{1}'',{2});");
                                coloumnResult = "update_user_country";
                                return mf.format(parsedParams);
                            } else {
                                if (params.has("profile_pic")) {
                                    parsedParams = new Object[] {
                                            params.get("id"), params.get("access_token"),
                                            params.getString("profile_pic")};
                                    mf = new MessageFormat("SELECT update_user_profile_pic({0}, ''{1}'',''{2}'');");
                                    coloumnResult = "update_user_profile_pic";
                                    return mf.format(parsedParams);
                                }

                            }
                        }
                    }
                }
            }
        }
//        Object[] parsedParams = new Object[] {
//                params.get("first_name"),
//                params.get("last_name"),
//                params.get("email"),
//                params.get("pass"),
//                params.get("age"),
//                params.get("country"),
//                params.get("profile_pic")};
        return null;
    }
}
