package users.commands;

import core.common.Command;
import core.dependencies.ReddisCluster;
import core.dependencies.SQLDBConnectionPool;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;

public class ShowUserCommand extends Command {

    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;

    public ShowUserCommand(SQLDBConnectionPool pool, ReddisCluster cluster, JSONObject params) {
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = cluster;
    }

    @Override
    public JSONObject call() throws Exception {
        JSONObject response;
        String users = reddisCluster.get(params.getString("id"));

        if (users != null) {
            JSONObject tmp = new JSONObject(users);
            System.out.println(" from REdddis ya prince");

            System.out.println(users);

            return new JSONObject()
                    .put("response", tmp)
                    .put("error", "")
                    .put("status", "true");
        }
        try {
            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString("show");
                        tmp2 = tmp2.substring(1, tmp2.length() - 1);
                        String[] tmps = tmp2.split(",");
                        tmp.put("id", tmps[0]);
                        tmp.put("first_name", tmps[1]);
                        tmp.put("last_name", tmps[2]);
                        tmp.put("email", tmps[3]);
                        tmp.put("password", tmps[4]);
                        tmp.put("age", tmps[5]);
                        tmp.put("country", tmps[6]);
                        tmp.put("prof_pic", tmps[7]);
                        tmp.put("access_token", tmps[8]);
                        reddisCluster.set(tmps[0], tmp.toString());
                        return new JSONObject()
                                .put("response", tmp)
                                .put("error", "")
                                .put("status", "true");
                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "User Not Found!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    exception.printStackTrace();
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.toString())
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", "User Not Found!")
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            response = new JSONObject()
                    .put("response", "")
                    .put("error", e.toString())
                    .put("status", "false");
        }

        return response;
    }

    private String getQueryString() {
        MessageFormat mf = new MessageFormat("SELECT show({0}, ''{1}'');");
        Object[] parsedParams = new Object[]{
                params.get("id"), params.get("access_token")};


        return mf.format(parsedParams);
    }
}