package users.commands;

import core.common.Command;
import core.common.RpcServer;
import core.dependencies.SQLDBConnectionPool;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;

public class LoginCommand extends Command {
    private SQLDBConnectionPool receiver;
    private JSONObject params;

    public LoginCommand(SQLDBConnectionPool pool, JSONObject params) {
        this.receiver = pool;
        this.params = params;
    }

    @Override
    public JSONObject call() {
        JSONObject response;
        try {
            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString("login");
                        tmp2 = tmp2.substring(1, tmp2.length()-1);
                        String[] tmps = tmp2.split(",");
                        tmp.put("id", tmps[0]);
                        tmp.put("access_token", tmps[1]);

                        return new JSONObject()
                                .put("response", tmp)
                                .put("error", "")
                                .put("status", "true");
                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "User Not Found!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    String error;
                    switch (RpcServer.errorReportingLevel) {
                        case 0:
                            error = exception.toString();
                            break;
                        case 1:
                            error = "Database failure";
                            break;
                        default:
                            error = "";
                    }
                    return new JSONObject()
                            .put("response", "")
                            .put("error", error)
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", "Bad data")
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            String error;
            switch (RpcServer.errorReportingLevel) {
                case 0:
                    error = e.toString();
                    break;
                case 1:
                    error = "Database failure";
                    break;
                default:
                    error = "";
            }
            response =  new JSONObject()
                    .put("response", "")
                    .put("error", error)
                    .put("status", "false");
        }

        return response;
    }

    private String getQueryString() {
        MessageFormat mf = new MessageFormat("SELECT login(''{0}'', ''{1}'');");
        Object[] parsedParams = new Object[] {
                params.get("email"), params.get("pass")};

        return mf.format(parsedParams);
    }
}
