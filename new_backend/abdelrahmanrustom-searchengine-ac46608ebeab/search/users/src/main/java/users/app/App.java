package users.app;

import admin.AdminServer;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

public class App {
    private static final Logger logger = Logger.getLogger(AdminServer.class.getName());

    private UsersRpcServer rpcServer;
    private AdminServer adminServer;

    public App(Properties properties) {
        logger.info(properties.toString());
        this.rpcServer = new UsersRpcServer(properties, properties.getProperty("name"));
        this.adminServer = new AdminServer(this.rpcServer, properties);
    }

    public void start() {
        try {
            this.rpcServer.start();
            this.adminServer.start();
        } catch (Exception exception) {
            logger.severe(exception.getLocalizedMessage());
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(App.class.getResourceAsStream("app.properties"));
        } catch (IOException exception) {
            logger.severe(exception.getLocalizedMessage());
            System.exit(0);
        }

        App app = new App(properties);
        app.start();
    }
}
