package core.util;

import com.rabbitmq.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class AsyncRabbitMQClient {
    private ConnectionFactory connectionFactory;
    private ExecutorService executorService;
    private Writer writer;
    private Reader reader;
    private String routeKey;
    private String replyToRouteKey;

    private static AtomicLong serialID = new AtomicLong(1);

    public AsyncRabbitMQClient(String routeKey) {
        this.routeKey = routeKey;
        connectionFactory = new ConnectionFactory();
        replyToRouteKey = AsyncRabbitMQClient.class.getSimpleName() + "#" + Long.toString(serialID.getAndIncrement());
    }

    public void start() throws IOException, TimeoutException {
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        writer = new Writer(connectionFactory, executorService);
        reader = new Reader(connectionFactory, executorService);
    }

    public CompletableFuture<byte[]> asyncCompletableRpc(byte[] data) {
        return reader.read(writer.write(data));
    }

    public void close() throws IOException {
        writer.close();
        writer = null;

        reader.close();
        reader = null;

        executorService.shutdown();
        executorService = null;
    }

    private abstract class ConnectionBroker {
        protected Connection connection;
        protected Channel channel;

        private ConnectionBroker(ConnectionFactory connectionFactory, ExecutorService executorService) throws IOException, TimeoutException {
            connection = connectionFactory.newConnection(executorService);
            channel = connection.createChannel();
            initQueue();
        }

        protected abstract void initQueue() throws IOException;

        protected AMQP.BasicProperties createProperties(String corrId) {
            AMQP.BasicProperties props = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(corrId)
                    .replyTo(replyToRouteKey)
                    .build();
            return props;
        }

        protected void close() throws IOException {
            connection.close();
        }
    }

    private final class Writer extends ConnectionBroker {
        private AtomicLong writeSerialID = new AtomicLong(1);
        private ExecutorService serialExecutorService;

        private Writer(ConnectionFactory connectionFactory, ExecutorService executorService) throws IOException, TimeoutException {
            super(connectionFactory, executorService);
            initSerialExecutorService();
        }

        @Override
        protected void initQueue() throws IOException {
            channel.queueDeclare(routeKey, false, false, true, null);
        }

        private void initSerialExecutorService() {
            serialExecutorService = Executors.newSingleThreadExecutor();
        }

        private CompletableFuture<Long> write(byte[] data) {
            CompletableFuture<Long> writeFuture = new CompletableFuture<>();
            serialExecutorService.submit(() -> {
                Long writeId = writeSerialID.getAndIncrement();
                AMQP.BasicProperties props = createProperties(Long.toString(writeId));
                try {
                    channel.basicPublish("", routeKey, props, data);
                    writeFuture.complete(writeId);
                } catch (IOException exception) {
                    writeFuture.completeExceptionally(exception);
                }
            });
            return writeFuture;
        }

        @Override
        protected void close() throws IOException {
            super.close();
            serialExecutorService.shutdown();
        }
    }

    private final class Reader extends ConnectionBroker {
        private ConcurrentMap<Long, CompletableFuture<byte[]>> futureMap;

        private Reader(ConnectionFactory connectionFactory, ExecutorService executorService) throws IOException, TimeoutException {
            super(connectionFactory, executorService);
            initFutureMap();
            initConsumer();
        }

        @Override
        protected void initQueue() throws IOException {
            channel.queueDeclare(replyToRouteKey, false, false, true, null);
        }

        private void initFutureMap() {
            futureMap = new ConcurrentHashMap<>();
        }

        private void initConsumer() throws IOException {
            channel.basicConsume(replyToRouteKey, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    CompletableFuture<byte[]> readFuture = futureMap.remove(Long.parseLong(properties.getCorrelationId()));
                    if(readFuture != null) {
                        readFuture.complete(body);
                    } else {
                        // Report some serious problem
                        // Received message which we didn't send
                        // Or we sent a message then lost it
                    }
                }
            });
        }

        private CompletableFuture<byte[]> read(CompletableFuture<Long> readKeyFuture) {
            CompletableFuture<byte[]> readFuture = new CompletableFuture<>();
            readKeyFuture
                    .thenAcceptAsync((readKey) -> futureMap.put(readKey, readFuture), executorService)
                    .exceptionally((exception) -> {
                        readFuture.completeExceptionally(exception);
                        return null;
            });
            return readFuture;
        }
    }

    public static void main(String[] args) throws Exception {
        AsyncRabbitMQClient asyncRabbitMQClient = new AsyncRabbitMQClient("Users");
        asyncRabbitMQClient.start();
        byte[] params= new JSONObject("{ \"ServiceCommand\": \"ShowUserCommand\", \"id\": 50, }")
                .toString().getBytes();
        CompletableFuture[] future = new CompletableFuture[1000];
        final AtomicInteger completed = new AtomicInteger(0);
        System.out.println("starting");
        Instant start = Instant.now();

        for(int i = 0; i < 1000; i++) {
            future[i] = asyncRabbitMQClient.asyncCompletableRpc(params).thenAccept((k) -> {
                completed.getAndIncrement();
            }).exceptionally((exceptioono) -> {
                exceptioono.printStackTrace();
                return null;
            });
        }
        CompletableFuture.allOf(future).whenComplete((k, v) -> {
            try {
                System.out.println(completed);
                Instant end = Instant.now();
                System.out.println(Duration.between(start, end).getSeconds());
                asyncRabbitMQClient.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
    }

}
