package core.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class ClassManager {
    private Map<String, Class<?>> classMap = new HashMap<>();
    private ClassLoader classLoader;

    private static final Logger logger = Logger.getLogger(ClassManager.class.getName());

    public ClassManager(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public ClassManager(ClassLoader classLoader, Properties classMap) {
        this(classLoader);
        loadClassMap(classMap);
    }

    public ClassManager(Properties classMap) {
        this(ClassManager.class.getClassLoader(), classMap);
    }

    private void loadClassMap(Properties classMap) {
        classMap.forEach((classKey, className) -> {
            try {
                Class loadedClass = classLoader.loadClass((String) className);
                this.classMap.put((String) classKey, loadedClass);
                logger.info("Loaded class: " + loadedClass.getSimpleName());
            } catch (ClassNotFoundException exception) {
                logger.warning("Failed to load class: " + className);
                logger.warning(exception.getLocalizedMessage());
                System.exit(0);
            }
        });
    }

    public void updateClass(String className) {
        try {
            Class loadedClass = classLoader.loadClass(className);
            classMap.put(loadedClass.getSimpleName(), loadedClass);
        } catch (ClassNotFoundException exception) {
            logger.warning("Failed to load class: " + className);
            logger.warning(exception.getLocalizedMessage());
            exception.printStackTrace();
        }
    }

    public void deleteClass(String className) {
        classMap.put(className, null);
    }

    public Class<?> classForName(String className) {
        return classMap.get(className);
    }

    public boolean containsClass(String className) {
        return classMap.containsKey(className);
    }
}
