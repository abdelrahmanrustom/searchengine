package core.common;

import com.rabbitmq.client.*;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.*;

public class RpcServer {
    protected ThreadPoolExecutor executorService;
    protected ExecutorCompletionService<Delivery> executorCompletionService;

    protected ConnectionFactory connectionFactory;
    protected Connection connection;
    protected Connection connectionPublish;
    protected Channel channel;
    protected Channel channelPublish;

    protected ConsumerWorker consumer;

    protected PublisherWorker publisher;
    protected Thread publisherThread;

    protected String host;
    protected static final String HOST_KEY = "host";
    protected static final String DEFAULT_HOST = "localhost";

    protected ClassManager classManager;
    protected DependencyManager dependencyManager;

    protected String queueName;

    public static Integer errorReportingLevel;

    public RpcServer(Properties properties, String name) {
        Properties classMap = new Properties();
        Properties depMap = new Properties();
        this.queueName = name;
        try {
            InputStream is = getClass().getResourceAsStream("classmap.properties");
            InputStream is2 = getClass().getResourceAsStream("dependencies.properties");
            classMap.load(is);
            depMap.load(is2);
        } catch (IOException exception) {
            exception.printStackTrace();
            System.exit(0);
        }
        System.out.println(classMap);
        classManager = new ClassManager(classMap);
        dependencyManager = new DependencyManager(depMap, classManager);
        initProperties(properties);
        initConnectionFactory();
        initExecutorCompletionService();
    }

    private void initProperties(Properties properties) {
        host = properties.getProperty(HOST_KEY, DEFAULT_HOST);
    }

    public void initConnectionFactory() {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
    }

    public void initExecutorCompletionService() {
        executorService = new ThreadPoolExecutor(10, 10,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
        executorCompletionService = new ExecutorCompletionService<>(executorService);
    }

    public void start() throws IOException, TimeoutException {
        connection = connectionFactory.newConnection(Executors.newSingleThreadExecutor((runnable) ->
                new Thread(Thread.currentThread().getThreadGroup(), runnable,"Consumer Thread")));
        channel = connection.createChannel();

        connectionPublish = connectionFactory.newConnection(Executors.newSingleThreadExecutor());
        channelPublish = connectionPublish.createChannel();

        consumer = new ConsumerWorker();

        publisher = new PublisherWorker();
        publisherThread = new Thread(publisher, "Publisher Thread");

        consumer.start();
        publisherThread.start();
    }

    public final class ConsumerWorker {
        private DefaultConsumer consumer = new RpcConsumer(channel);

        public void start() {
            unfreeze();
        }

        public void freeze() {
            try {
                channel.basicCancel(consumer.getConsumerTag());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        public void unfreeze() {
            try {
                channel.queueDeclare(queueName, false, false, true, null);
                channel.basicConsume(queueName, true, consumer);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }



    private final class RpcConsumer extends DefaultConsumer {
        RpcConsumer(Channel channel) {
            super(channel);
        }

        private Delivery dispatchDelivery(Delivery delivery) {
            JSONObject jsonObject = new JSONObject(new String(delivery.getBody(), CharsetUtil.UTF_8));
            byte[] reply;
            try {
                final Command command = CommandParser.parseCommand(classManager, dependencyManager, jsonObject);
                reply = command.call().toString().getBytes();
            } catch(Exception e) {
                JSONObject res = new JSONObject()
                        .put("response", "")
                        .put("error", "bad request command unknown")
                        .put("status", "false");

                reply = res.toString().getBytes();
            }
            // System.out.println(new String(reply));
            return new Delivery(delivery.getEnvelope(), delivery.getProperties(), reply);
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            Future<Delivery> future = executorCompletionService.submit(() ->
                    dispatchDelivery(new Delivery(envelope, properties, body)));
        }
    }

    private void publishDelivery(Delivery delivery) throws IOException {
        AMQP.BasicProperties requestProperties = delivery.getProperties();
        String correlationId = requestProperties.getCorrelationId();
        String replyTo = requestProperties.getReplyTo();
        AMQP.BasicProperties.Builder replyPropertiesBuilder
                = new AMQP.BasicProperties.Builder().correlationId(correlationId);
        AMQP.BasicProperties replyProperties = replyPropertiesBuilder.build();
        channelPublish.basicPublish("", replyTo, replyProperties, delivery.getBody());
    }

    public final class PublisherWorker implements Runnable {
        private boolean isRunning = false;

        @Override
        public void run() {
            isRunning = true;
            while (isRunning) {
                try {
                    Delivery response = executorCompletionService.take().get();
                    publishDelivery(response);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }

        public void freeze() {
            isRunning = false;
        }
    }
}
