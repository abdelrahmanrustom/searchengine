package core.common;

import org.json.JSONObject;

import java.lang.reflect.Constructor;

public class CommandParser {
    private static final String COMMAND_NAME_KEY = "ServiceCommand";

    private CommandParser() {}

    public static Command parseCommand(ClassManager classManager, DependencyManager dependencies, JSONObject object) throws Exception {
        String commandName = object.getString(COMMAND_NAME_KEY);
        if(classManager.containsClass(commandName)) {
            Class cls = classManager.classForName(commandName);
            Constructor constructor = cls.getDeclaredConstructors()[0];
            Class[] parameterTypes = constructor.getParameterTypes();
            Object[] params = new Object[parameterTypes.length];
            for (int i = 0; i < parameterTypes.length; i++) {
                if (dependencies.containsKey(parameterTypes[i])) {
                    params[i] = dependencies.get(parameterTypes[i]);
                } else if (parameterTypes[i].equals(JSONObject.class)) {
                    params[i] = object;
                } else {
                    throw new Exception("Exception");
                }
            }
            return (Command) constructor.newInstance(params);
        } else {
            throw new Exception("Illegal Command");
        }
    }
}
