package core.dependencies;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.support.ConnectionPoolSupport;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

public class ReddisCluster {
    static GenericObjectPool<StatefulRedisConnection<String, String>> rcc;
    static {
        RedisURI redisUri = RedisURI.Builder.sentinel("sentinel", "mymaster").build();
        RedisClient client = RedisClient.create(redisUri);

        rcc = ConnectionPoolSupport
                .createGenericObjectPool(client::connect, new GenericObjectPoolConfig());
    }

    public void set(String key, String value) throws Exception{
        try (StatefulRedisConnection<String, String> connection = rcc.borrowObject()) {
            connection.sync().set(key, value);
        }
    }


    public String get(String key) throws Exception {
        String value;
        try (StatefulRedisConnection<String, String> connection = rcc.borrowObject()) {
            value = connection.sync().get(key);
        }
        return value;
    }

    public void delete(String key) throws Exception {
        try (StatefulRedisConnection<String, String> connection = rcc.borrowObject()) {
            connection.sync().del(key);
        }
    }


}
