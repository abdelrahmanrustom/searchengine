package core.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DependencyManager {

    Map<Class, Object> depMap;
    ClassManager clsMgr;

    public DependencyManager(Properties properties, ClassManager clsMgr) {
        depMap = new HashMap<>();
        this.clsMgr = clsMgr;
        properties.forEach((k, v) -> {
            Class cls = clsMgr.classForName((String) v);
            try {
                depMap.put(cls, cls.getConstructor().newInstance());
            } catch (Exception exception) {
                exception.printStackTrace();
                System.exit(0);
            }
        });
    }

    public DependencyManager() {
        this.depMap = new HashMap<>();
    }

    public void add(Object dep) {
        this.depMap.put(dep.getClass(), dep);
    }

    public boolean containsKey(Class cls) {
        if(depMap.containsKey(cls)) {
            return true;
        }
        for(Class key : depMap.keySet()) {
            if(cls.isAssignableFrom(key)) {
                return true;
            }
        }
        return false;
    }

    public Object get(Class cls) {
        if(depMap.get(cls) != null) {
            return depMap.get(cls);
        }
        for(Class key : depMap.keySet()) {
            if(cls.isAssignableFrom(key)) {
                return depMap.get(key);
            }
        }
        return null;
    }
}
