CREATE TABLE Users(
    _id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    pass VARCHAR(50) NOT NULL,
    age INTEGER,
    country INTEGER NOT NULL,
    profile_pic VARCHAR(50),
    access_token text NOT NULL default md5(random()::text)
    );
    