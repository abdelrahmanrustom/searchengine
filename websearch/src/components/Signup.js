import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { signupUser } from '../actions';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import { Input, Button, FormGroup,Form, FormFeedback, Label } from 'reactstrap';
import Img from 'react-image'

//import cloudinary from 'cloudinary-react';
class Signup extends Component {

  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    age: '',
    country: 'Egypt',
    profile_image: ''
  };

   constructor() {
    super();
    this.onButtonClicked = this.onButtonClicked.bind(this);
  }
uploadWidget() {
  console.log("result");
        window.cloudinary.openUploadWidget({ cloud_name: 'doodle-search', upload_preset: 'd7ixrutx', tags:['xmas']},
            function(error, result) {
                console.log(result);
            });
    }

handleDrop = files => {
  // Push all the axios request promise into a single array
  const uploaders = files.map(file => {
    // Initial FormData
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "d7ixrutx"); // Replace the preset name with your own
    formData.append("api_key", "999741735925526"); // Replace API key with your own Cloudinary key
    formData.append("timestamp", (Date.now() / 1000) | 0);
     console.log("beforee");
    // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
    return axios.post("https://api.cloudinary.com/v1_1/doodle-search/image/upload", formData, {
      headers: { "X-Requested-With": "XMLHttpRequest" },
    }).then(response => {
      const data = response.data;
      const fileURL = data.secure_url // You should store this URL for future references in your app
      console.log(data);
      this.setState({profile_image: fileURL});
    })
});
};

renderImage (){
 // console.log("heyy befor render");
  if (this.state.profile_image !== "") {

  console.log(this.state.profile_image);
 // console.log(this.state.profile_image.replace('https://res.cloudinary.com/doodle-search/image/upload/',''));
  return (

  <Img src= {this.state.profile_image} style = {style.imageStyle}/>
 )
  } else {
  return (
    <Dropzone 
    onDrop={this.handleDrop} 
    multiple 
    accept="image/*" >
      <p>Drop your files or click here to upload</p>
    </Dropzone>)
  }
}

 render() {
return (
	<div style = {{minHeight: '100vh', backgroundColor : '#fafafa'}}>
  

	<Form>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="firstname">First name</Label>
          <Input placeholder="First name" onChange={firstName => this.setState({ firstName: firstName.target.value })} className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="lastname">Last name</Label>
          <Input placeholder="Last name" onChange={lastName => this.setState({ lastName: lastName.target.value })} className="text-center " />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="email">Email</Label>
          <Input placeholder="Email"  onChange={email => this.setState({ email: email.target.value })} className="text-center " />
          <FormFeedback >Sweet! this email is available</FormFeedback>
          <FormFeedback >Sorry! this email already exists</FormFeedback>
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="password">Password</Label>
          <Input placeholder="Password" type = "password" onChange={password => this.setState({ password: password.target.value })} className="text-center " />
    </FormGroup>	
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="age">Age</Label>
          <Input placeholder="Age"  type = "number" onChange={age => this.setState({ age: age.target.value })} className="text-center " />
    </FormGroup>
   
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="firstname">Country</Label>
          <Input placeholder="Country" value = {this.state.country} className="text-center " />
    </FormGroup>



<FormGroup style = {style.inputStyle}>
{this.renderImage()}
 </FormGroup>
   <Button  style = {style.inputStyle} onClick = {this.onButtonClicked}>Signup</Button>
        </Form>
	</div>
	);
	}
   onButtonClicked() {
    const { firstName, lastName, email, password, age, country, profile_image } = this.state;
    //console.log(firstName +" "+ lastName+ " "+ email+ " "+ password+" "+ age+ " "+ country + " "+ profile_image);
    this.props.signupUser(firstName, lastName, email, password, age, "1", profile_image);
  }
}


const style = {
  
  inputStyle: {
    width: 500, marginLeft:400
},
imageStyle: {
    width: 200, height:200
}
}

const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading } = auth;
  return { email, password, error, loading };
};


 // <FormGroup style = {style.inputStyle}>
 //         <Label className="align-center " for="profilepicture">Profile picture</Label>
 //         <Dropzone onDrop={this.handleDrop} accept = "image/*" style= {style.dropzone}>
 //         <p>Drop our files or click here to upload</p>
 //         </Dropzone>
 //   </FormGroup>

export default connect(mapStateToProps, { signupUser } )(Signup);