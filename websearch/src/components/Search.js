import React, { Component } from 'react';
import { Form,
 Button, 
 FormGroup, 
 Input, Nav, 
 NavItem, 
 NavLink,
 ListGroup, 
 ListGroupItem, 
 ListGroupItemHeading, 
 ListGroupItemText,
 ButtonDropdown, 
 DropdownToggle, 
 DropdownMenu, 
 DropdownItem,
} from 'reactstrap';
import { connect } from 'react-redux';
import { search } from '../actions';

class Search extends Component {
  state = {
    resultType: 0,
    q: "",
    page_no : 1,
    dropdownOpen: false,
    value : "All categories",
    category_id: -1,
    pages_no: 20,
    page: 1
  };

constructor() {
    super();
    this.onImagesClicked = this.onImagesClicked.bind(this);
    this.onVideosClicked = this.onVideosClicked.bind(this);
    this.onLinkClicked = this.onLinkClicked.bind(this);
    this.toggle = this.toggle.bind(this);
    this.catogriesClicked = this.catogriesClicked.bind(this);
    this.pagesClicked = this.pagesClicked.bind(this);
     
  }

   componentWillMount() {
//console.log(this.props.location.state.search);
 const { resultType, q, page_no, category_id } = this.state;

this.setState({q: this.props.location.state.search});
this.props.search({ resultType, q:this.props.location.state.search, page_no, category: category_id });
   }

    toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

catogriesClicked(event){  
  console.log(event.target.id);

   this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      value: event.target.innerText,
      category_id: event.target.id
    });
}

pagesClicked(event){  

   this.setState({
      page: event.target.id
    });
  
}

  renderNav(){
 // console.log("heyy befor render");

    if (this.state.resultType === 0) {
    return (
    <Nav color="light">
          <NavLink onClick = {this.onLinkClicked}  disabled href="#" style = {style.navLinkStyle}>Link</NavLink> 
          <NavLink onClick = {this.onImagesClicked} href="#" style = {style.navLinkStyle}>Images</NavLink> 
          <NavLink onClick = {this.onVideosClicked}  href="#" style = {style.navLinkStyle}>Videos</NavLink>
        </Nav>
    )  
    } else {
      if (this.state.resultType === 1) {
    return (
    <Nav color="light">
          <NavLink onClick = {this.onLinkClicked}   href="#" style = {style.navLinkStyle}>Link</NavLink> 
          <NavLink onClick = {this.onImagesClicked} disabled href="#" style = {style.navLinkStyle}>Images</NavLink> 
          <NavLink onClick = {this.onVideosClicked}  href="#" style = {style.navLinkStyle}>Videos</NavLink>
        </Nav>
    )
    } else {
      if (this.state.resultType === 2) {
    return (
    <Nav color="light">
          <NavLink onClick = {this.onLinkClicked}    href="#" style = {style.navLinkStyle}>Link</NavLink> 
          <NavLink onClick = {this.onImagesClicked} href="#" style = {style.navLinkStyle}>Images</NavLink> 
          <NavLink onClick = {this.onVideosClicked} disabled href="#" style = {style.navLinkStyle}>Videos</NavLink>
        </Nav>
    )
    }
  }
}
}

renderPagination(){
var photoHolder = [];
    for(var i=0;i<this.state.pages_no;i++){
        photoHolder.push(
            (
                <NavLink id = {i+1} onClick = {this.pagesClicked}  href="#" style = {style.navLinkStyle}>{i+1}</NavLink>
            )

        );  
    }
return photoHolder;
}
  // resultType 0 -> links  1 ->Images 2->Videos
 render() {

//console.log(this.props.location.state.search);
return (
  <div>
<Form inline style = {{backgroundColor : '#fafafa'}}>

<FormGroup style = {{marginTop:10}}>
<img src={require("./Capture3.PNG")}  style = {style.imageStyle} width="15%"  />
        <Input  style = {style.textStyle} onChange={q => this.setState({ q: q.target.value })} value = {this.state.q}  placeholder="Search"/>
         <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret>
          {this.state.value}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem id = "-1" onClick= {this.catogriesClicked} >All categories</DropdownItem>
          <DropdownItem id = "1" onClick= {this.catogriesClicked} >Arts</DropdownItem>
          <DropdownItem id = "2" onClick= {this.catogriesClicked}>Business</DropdownItem>
          <DropdownItem id = "3" onClick= {this.catogriesClicked}>Computers</DropdownItem>
          <DropdownItem id = "4" onClick= {this.catogriesClicked}>Games</DropdownItem>
          <DropdownItem id = "5" onClick= {this.catogriesClicked}>Health</DropdownItem>
          <DropdownItem id = "6" onClick= {this.catogriesClicked}>Home</DropdownItem>
          <DropdownItem id = "7" onClick= {this.catogriesClicked}>News</DropdownItem>
          <DropdownItem id = "8" onClick= {this.catogriesClicked}>Recreation</DropdownItem>
          <DropdownItem id = "9" onClick= {this.catogriesClicked}>Reference</DropdownItem>
          <DropdownItem id = "10" onClick= {this.catogriesClicked}>Science</DropdownItem>
          <DropdownItem id = "11" onClick= {this.catogriesClicked}>Shopping</DropdownItem>
          <DropdownItem id = "12" onClick= {this.catogriesClicked}>Society</DropdownItem>
          <DropdownItem id = "13" onClick= {this.catogriesClicked}>Sports</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
        <img src={require("./Capture3.PNG")}  style = {style.imageStyle} className="float-right" height="10%" width="10%" />
</FormGroup>

{this.renderNav()}
        </Form>

        <ListGroup>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
        <ListGroupItem>
          <ListGroupItemHeading>
           <a href="https://www.w3schools.com">Visit W3Schools</a>
          </ListGroupItemHeading>
          <ListGroupItemText>
          Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
          </ListGroupItemText>
        </ListGroupItem>
      </ListGroup>
      <Form inline style = {{backgroundColor : '#fafafa'}}>
        <FormGroup >
        {this.renderPagination()}
        </FormGroup>
        </Form>
      </div>
);
}


 onImagesClicked() {
  this.setState({resultType: 1});
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    console.log("images");
    //this.props.loginUser({ "email":"email", "password":"password" });
  }

onVideosClicked() {
  this.setState({resultType: 2});
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    console.log("videosss");
    //this.props.loginUser({ "email":"email", "password":"password" });
  }

onLinkClicked() {
  this.setState({resultType: 0});
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    console.log("linkss");
    //this.props.loginUser({ "email":"email", "password":"password" });
  }
}

const style = {
  
  textStyle: {
    alignSelf: 'center',
    fontSize: 16,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft:5,
    paddingRight:300
  },
  imageStyle: {
    paddingTop: 10,
    paddingLeft: 15,
    backgroundColor:'#00000000',
    opacity: 0.6
  },
  navLinkStyle:{
    marginLeft:15
  }
};


const mapStateToProps = ({ search }) => {
  const { searches } = search;
  return { searches };
};

export default connect(mapStateToProps, { search } )(Search);



// <Container>
// <Row>
// <Col  xs="6" sm="4"><img src={require("./Capture3.PNG")}  width="15%" /> </Col>
// <Col  xs="6" sm="4"><Input  placeholder="username"/> </Col>
// </Row>
// <Row>
// <Col  xs="6" sm="4"> 
// <Nav>
//           <NavItem>
//             <NavLink href="#">Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink href="#">Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink href="#">Another Link</NavLink>
//           </NavItem>
//           <NavItem>
//             <NavLink disabled href="#">Disabled Link</NavLink>
//           </NavItem>
//         </Nav>
// </Col>

// </Row>
// </Container>
