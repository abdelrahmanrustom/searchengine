import React, { Component } from 'react';
import './css/home.css'
//import { matchPath } from 'react-router'
import { Link } from 'react-router-dom';
import Img from 'react-image';
import { Button } from 'reactstrap';
import {
  USER_ID,
  ACCESS_TOKEN,
  URL_PIC,
  PROFILE_PIC,
  AGE,
  COUNTRY,
  EMAIL,
  FIRST_NAME,
  LAST_NAME,
  PASS
} from '../utils/data'

class Home extends Component {

// const Home = () => {

constructor(props){
   super(props);
   //this.state={inputfield: "no value"};   
    this.state = { search_text: "", signed: false };
 // this.doodleSearchClick = this.doodleSearchClick.bind(this);
   this.searchText = this.searchText.bind(this);
   this.onButtonClicked = this.onButtonClicked.bind(this);
   //TODO i am feeling lucky
  }

  
  

  componentWillMount() {
     const cachedHits = localStorage.getItem(USER_ID);
     if (cachedHits) {
        console.log("id is equal to "+ cachedHits);
        console.log("auth key is equal to  "+ localStorage.getItem(ACCESS_TOKEN));
        this.setState({signed : true});
     } else {
      //localStorage.setItem(key, JSON.stringify(result.hits));
      localStorage.setItem("hello", "world");
      console.log("hello value is empty");
     }
  }

  onButtonClicked() {
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    // console.log(this.state.email);
      localStorage.removeItem(USER_ID);
      localStorage.removeItem(ACCESS_TOKEN);
      localStorage.removeItem(AGE);
      localStorage.removeItem(COUNTRY);
      localStorage.removeItem(EMAIL);
      localStorage.removeItem(FIRST_NAME);
      localStorage.removeItem(LAST_NAME);
      localStorage.removeItem(PROFILE_PIC);
      localStorage.removeItem(PASS);
      this.setState({signed : false});
      console.log("eshta");
  }
  


      

searchText(evt) {
   
    this.setState({search_text: evt.target.value});
    //this.props.search = evt.target.value;
  }

  renderImageAndSignin(){
      if (!this.state.signed) {
          console.log(true);
        return (<Link to = {{ pathname: '/signin'}} > <li className="nav-links"><a href="#">Sign In</a></li> </Link>);
      } 
      else {
         console.log(false);
        return(<div>
          <Link  to = {{ pathname: '/profile'}} >
          <img id = "image" src={URL_PIC+localStorage.getItem(PROFILE_PIC)}  width="15%"  /></Link>
           <Button   onClick = {this.onButtonClicked}>Sign out</Button>
           </div>
          );
      }
  }


// doodleSearchClick(){
//   // this.props.router.push('')
//    console.log(this.state.search_text); 
// }  



// <FormGroup row style = {style.inputStyle}>
//           <Img id = "image"  src= "https://www.w3schools.com/images/w3schools_green.jpg" style = {style.imageStyle}/>
//         </FormGroup>
  render() {
return (
  <body>
    <header>
    
          
          {this.renderImageAndSignin()}
      
    </header>  
    
    <div className="google">
      <a href="#" id="google_logo"><img src={require("./Capture3.PNG")}/></a>
    </div>
    
    <div className="form">  
      <form>
        <label for="form-search"></label>
        <input type="text" id="form-search" placeholder="Search doodle or type URL" onChange={this.searchText} />
      </form>
    </div>  
    
    <div className= "buttons">  
    <Link to = {{ pathname: '/search', state: { search: this.state.search_text} }} ><input type="submit" value="Doodle Search"/></Link>
      
      <input type="submit" value="I'm Feeling Lucky" id="im_feeling_lucky"/>
    </div>
    <footer>
        <ul className="footer-left">
          <li><a href="#">Advertising</a></li>
          <li><a href="#">Business</a></li>
          <li><a href="#">About</a></li> 
        </ul>
        <ul className="footer-right">    
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Terms</a></li>
          <li><a href="#">Settings</a></li>
        </ul>       
    </footer>      
  </body>
	);
}
}


export default Home ;