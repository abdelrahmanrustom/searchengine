import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Img from 'react-image'
import './css/Userhome.css'
import { getUser, editUser } from '../actions';
import { connect } from 'react-redux';
import { Input, Button, FormGroup,Form, FormFeedback, Label, Col, Media } from 'reactstrap';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import {
  USER_ID,
  ACCESS_TOKEN,
  URL_PIC
} from '../utils/data'

class Userhome extends Component {
 state = {
    email: false,
    password: false,
    first_name: false,
    last_name: false,
    age: false,
    change_profile_image: false,
    new_profile_image: "",
    firstName: '',
    lastName: '',
    emailNew: '',
    passwordNew: '',
    ageNew: '',
    country: 'Egypt',
  };


  
  componentWillMount() {
//console.log(this.props.location.state.search);

  this.props.getUser({ "id":localStorage.getItem(USER_ID), "access_token":localStorage.getItem(ACCESS_TOKEN) });
  }

   constructor() {
    super();
    this.emailClicked = this.emailClicked.bind(this);
    this.passwordClicked = this.passwordClicked.bind(this);
    this.firstnameClicked = this.firstnameClicked.bind(this);
    this.lastnameClicked = this.lastnameClicked.bind(this);
    this.ageClicked = this.ageClicked.bind(this);
    this.onButtonClickedEditUser = this.onButtonClickedEditUser.bind(this);
  }

 emailClicked() {
    const { email } = this.state;
    if (email) {
      this.setState({email: false});
    } else {
      this.setState({password: false, email: true, first_name: false, last_name: false, age: false});
    }
  }

  onButtonClickedEditUser(){
    const { firstName, lastName, emailNew, passwordNew, ageNew, country, new_profile_image } = this.state;
    // console.log(new_profile_image);
    this.props.editUser({ 
      "id":localStorage.getItem(USER_ID),
     "access_token":localStorage.getItem(ACCESS_TOKEN),
      "firstName" :firstName,
      "lastName" : lastName,
      "email": emailNew, 
      "password": passwordNew, 
      "age": ageNew, 
      country: '', 
      "profile_image": new_profile_image  });
  }

firstnameClicked(){
 const { first_name } = this.state;
    if (first_name) {
      this.setState({first_name: false});
    } else {
      this.setState({password: false, email: false, first_name: true, last_name: false, age: false});
    }
}

lastnameClicked() {
 const { last_name } = this.state;
    if (last_name) {
      this.setState({last_name: false});
    } else {
      this.setState({password: false, email: false, first_name: false, last_name: true, age: false});
    }
}

ageClicked() {
 const { age } = this.state;
    if (age) {
      this.setState({age: false});
    } else {
      this.setState({password: false, email: false, first_name: false, last_name: false, age: true});
    }
}

 passwordClicked() {
    const { password } = this.state;
    if (password) {
      this.setState({password: false});
    } else {
      this.setState({password: true, email: false, first_name: false, last_name: false, age: false});
    }
  }

  renderEditEmail(){
        if (this.state.email) {
        return (<FormGroup row style = {style.inputStyle}>
          <Col sm={10}>
          <Input placeholder="Edit email" onChange={emailNew => this.setState({ emailNew: emailNew.target.value })} className="text-center " />
          </Col>
          <Button onClick = {this.onButtonClickedEditUser} >Update</Button>
        </FormGroup>);
      }
  }
  renderEditFirstname(){
        if (this.state.first_name) {
        return (<FormGroup row style = {style.inputStyle}>
          <Col sm={10}>
          <Input placeholder="Edit first name" onChange={firstName => this.setState({ firstName: firstName.target.value })} className="text-center " />
          </Col>
          <Button onClick = {this.onButtonClickedEditUser}>Update</Button>
        </FormGroup>);
      }
  }

  renderEditLastname(){
        if (this.state.last_name) {
        return (<FormGroup row style = {style.inputStyle}>
          <Col sm={10}>
          <Input placeholder="Edit last name" onChange={lastName => this.setState({ lastName: lastName.target.value })} className="text-center " />
          </Col>
          <Button onClick = {this.onButtonClickedEditUser}>Update</Button>
        </FormGroup>);
      }
  }


  renderEditAge(){
        if (this.state.age) {
        return (<FormGroup row style = {style.inputStyle}>
          <Col sm={10}>
          <Input placeholder="Edit age" onChange={age => this.setState({ ageNew: age.target.value })} className="text-center " />
          </Col>
          <Button onClick = {this.onButtonClickedEditUser}>Update</Button>
        </FormGroup>);
      }
  }
  renderEditPassword() {
        if (this.state.password) {
        return (<FormGroup row style = {style.inputStyle}>
          <Col sm={10}>
          <Input placeholder="Edit password" onChange={passwordNew => this.setState({ passwordNew: passwordNew.target.value })} className="text-center " />
          </Col>
          <Button onClick = {this.onButtonClickedEditUser} >Update</Button>
        </FormGroup>);
      }
  }


handleDrop = files => {
  // Push all the axios request promise into a single array
  const uploaders = files.map(file => {
    // Initial FormData
    const formData = new FormData();
    formData.append("file", file);
    formData.append("upload_preset", "d7ixrutx"); // Replace the preset name with your own
    formData.append("api_key", "999741735925526"); // Replace API key with your own Cloudinary key
    formData.append("timestamp", (Date.now() / 1000) | 0);
     console.log("beforee");
    // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
    return axios.post("https://api.cloudinary.com/v1_1/doodle-search/image/upload", formData, {
      headers: { "X-Requested-With": "XMLHttpRequest" },
    }).then(response => {
      const data = response.data;
      const fileURL = data.secure_url // You should store this URL for future references in your app
      console.log(data);

      const { firstName, lastName, emailNew, passwordNew, ageNew, country } = this.state;
    // console.log(new_profile_image);
    this.props.editUser({ 
      "id":localStorage.getItem(USER_ID),
     "access_token":localStorage.getItem(ACCESS_TOKEN),
      "firstName" :firstName,
      "lastName" : lastName,
      "email": emailNew, 
      "password": passwordNew, 
      "age": ageNew, 
      country: '', 
      "profile_image": fileURL  });

      this.setState({new_profile_image: fileURL});
    })
});
};

renderImage (){
 // console.log("heyy befor render");
  if (!this.state.change_profile_image) {
  return (
    <Dropzone 
    onDrop={this.handleDrop} 
    multiple 
    accept="image/*" >
      <p>Drop your files or click here to upload</p>
    </Dropzone>)
    this.setState({change_profile_image: true})
}
}

 render() {
  console.log(this.props.user);
  var email = null;
  var first_name = null;
  var last_name = null;
  var age = null;
  var profile_pic = null;
  
  if (this.props.user !== null) {
    if (this.props.user.email !== null) {
       email = this.props.user.email;
    first_name = this.props.user.first_name;
    last_name = this.props.user.last_name;
    age = this.props.user.age;
    profile_pic = URL_PIC+this.props.user.profile_pic;
    console.log(profile_pic);
    }
  }
return (
  <div style = {{minHeight: '100vh', backgroundColor : '#fafafa'}}>
        <Form>
        <FormGroup row style = {style.inputStyle}>
          <Img id = "image"  src= {profile_pic} style = {style.imageStyle}/>
        </FormGroup>
        <FormGroup row style = {style.inputStyle}>
          <Label className="align-center " for="email" sm={2} >Email</Label>
          <Col sm={10}>
          <Button  style = {style.buttonStyle}  bsSize="lg" onClick = {this.emailClicked}>{email}</Button>
          </Col>
        </FormGroup>
        {this.renderEditEmail()}
        <FormGroup row style = {style.inputStyle}>

          <Label className="align-center" for="password" sm={2} >Password</Label>
          <Col sm={10}>
          <Button  style = {style.buttonStyle}  bsSize="lg" onClick = {this.passwordClicked}>**********</Button>
          </Col>
        </FormGroup>
        {this.renderEditPassword()}
         <FormGroup row style = {style.inputStyle}>

          <Label className="align-center"  sm={2} for="first_name">Firstname</Label>
          <Col sm={8}>
          <Button  style = {style.buttonStyle}  bsSize="lg" onClick = {this.firstnameClicked}>{first_name}</Button>
          </Col>
        </FormGroup>
        {this.renderEditFirstname()}
        <FormGroup row style = {style.inputStyle}>

          <Label className="align-center"  sm={2} for="last_name">Lastname</Label>
          <Col sm={8}>
          <Button  style = {style.buttonStyle}  bsSize="lg" onClick = {this.lastnameClicked}>{last_name}</Button>
          </Col>
        </FormGroup>
        {this.renderEditLastname()}
        <FormGroup row style = {style.inputStyle}>

          <Label className="align-center"  sm={2} for="age">Age</Label>
          <Col sm={8}>
          <Button  style = {style.buttonStyle}  bsSize="lg" onClick = {this.ageClicked}>{age}</Button>
          </Col>
        </FormGroup>
        {this.renderEditAge()}
          <FormGroup row style = {style.inputStyle}>
        {this.renderImage()}
        </FormGroup>
        <FormGroup row style = {style.inputStyle}>
        <Link to = {{ pathname: '/'}} > <img src={require("./Capture3.PNG")} width="50%"  /></Link>
        </FormGroup>
      </Form>
  </div>
  );
}
}

const style = {
  
  inputStyle: {
    width: 500, 
    marginLeft:400
  },
  buttonStyle: {
    backgroundColor : '#ffffff',
    color: '#000000',
    width: 400
  },
  imageStyle: {
   marginLeft: 230,
   marginTop: 30
  }

}


const mapStateToProps = ({ auth }) => {
  const { user } = auth;
  return { user };
};

export default connect(mapStateToProps, { getUser, editUser } )(Userhome);

// <Form inline>
//         <FormGroup>
//           <Label for="exampleEmail" hidden>Email</Label>
//           <Input type="email" name="email" id="exampleEmail" placeholder="Email" />
//         </FormGroup>
//         {' '}
//         <FormGroup>
//           <Label for="examplePassword" hidden>Password</Label>
//           <Input type="password" name="password" id="examplePassword" placeholder="Password" />
//         </FormGroup>
//         {' '}
//         <Button>Submit</Button>
//       </Form>