import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginUser } from '../actions';
//https://www.npmjs.com/package/react-dialog for dialog
import { Input, Button, FormGroup,Form, FormFeedback, Label } from 'reactstrap';
class Signin extends Component {
  state = {
    email: '',
    password: ''
  };  
  constructor() {
    super();
    this.onButtonClicked = this.onButtonClicked.bind(this);
  }
 render() {
  const { email, password} = this.state;
  if (this.props.user) {
    return <Redirect push to="/profile" />;
  }
return (
	<div style = {{minHeight: '100vh', backgroundColor : '#fafafa'}}>
	<Form>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="email">Email</Label>
          <Input placeholder="Email" className="text-center "  onChange={email => this.setState({ email: email.target.value })} />
    </FormGroup>
	<FormGroup style = {style.inputStyle}>
          <Label className="align-center " for="password"  >Password</Label>
          <Input placeholder="Password"  type = "password" onChange={password => this.setState({ password: password.target.value })} className="text-center " />
    </FormGroup>
        <Button  style = {style.inputStyle} onClick = {this.onButtonClicked}>Sign in</Button>
        <Link  to = {{ pathname: '/signup'}} ><a style = {{marginLeft: 10}} href="#">Sign up</a> </Link>
          
        </Form>
	</div>
	);
}

 onButtonClicked() {
    // const { loginUser } = this.props;
    // loginUser({ "email", "password" });
    //const { loginUser } = this.props;
    // console.log(this.state.email);
    console.log(this.state.password);
    this.props.loginUser({ "email":this.state.email, "pass":this.state.password });
  }
}

const style = {
  
  inputStyle: {
    width: 500, marginLeft:400
}
}





const mapStateToProps = ({ auth }) => {
  const { email, password, user} = auth;
  return { email, password, user};
};

export default connect(mapStateToProps, { loginUser } )(Signin);