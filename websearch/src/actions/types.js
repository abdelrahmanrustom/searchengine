export const LOGIN = 'login';
export const LOGINFAIL = 'login_fail';
export const LOGIN_START = 'login_start';
export const SIGNUP = 'sign_up';
export const EDIT = 'edit';
export const SHOWUSER = 'show_user';
export const SEARCH =  'search';