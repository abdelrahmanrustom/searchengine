import {
  loginUserCall,
  signUpCall,
  getUserCall,
  editCall
} from './AuthActions';
import {
 searchCall
} from './SearchActions';

export const loginUser = ({ email, pass }) => {
	return loginUserCall({ email, pass });
};

export const signupUser = (firstName, lastName, email, password, age, country, profile_image) => {
	return signUpCall(firstName, lastName, email, password, age, country, profile_image);
};


export const getUser = (id , access_token) => {
	return getUserCall(id, access_token);
};


export const editUser = ({id , access_token, firstName, lastName, email, password, age, country, profile_image} ) => {
	//console.log("id" + id + " acces: "+ access_token+ " firs "+ firstName+ " lastName:" + lastName);

	return editCall({id, access_token, firstName, lastName, email, password, age, country, profile_image });
};

export const search = ({ resultType, q, page_no, category }) => {
	return searchCall({ resultType, q, page_no, category })
};