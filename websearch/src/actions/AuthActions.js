import axios from 'axios';
import {
  LOGIN_START,
  LOGINFAIL, 
  LOGIN,
  SIGNUP,
  EDIT,
  SHOWUSER
} from './types';

import {
  BASE_URL,
  USER_ID,
  ACCESS_TOKEN,
  URL_PIC,
  AGE,
  COUNTRY,
  EMAIL,
  FIRST_NAME,
  LAST_NAME,
  PASS,
  PROFILE_PIC
} from '../utils/data';

export const loginUserCall = ({ email, pass }) => {
  
	return dispatch => {
		dispatch({ type: LOGIN_START });
	  axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "LoginCommand",
   "email": email,
"destination service": "Users",
   "pass": pass }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }
      console.log(data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      logInUser(dispatch, data.response);
    })
    .catch(error =>  console.log(error));
  };
};


//938840e1d0f18f42fc154505dfd372e34



export const signUpCall = (firstName = '', lastName = '', email = '', password = '', age = '', country = '', profile_image = '') => {
  console.log(firstName +" "+ lastName+ " "+ email+ " "+ password+" "+ age+ " "+ country + " "+ profile_image.replace(URL_PIC,''));
  return dispatch => {
    dispatch({ type: SIGNUP });
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "SignUpCommand",
   "email": email,
   "destination service": "Users",
   "pass": password,
    "age": age,
    "country": country,
    "first_name": firstName,
   "last_name": lastName,
   "profile_pic": profile_image.replace(URL_PIC,'')}))
   .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }

      // const user = data.user;
      // user.token = data.token;
      console.log(data);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      logInUser(dispatch, data);
      //dispatch(loginUserCall({ email, password }));
    })
    .catch(error => { loginFail(dispatch, 'Sign up failed'); });
  };
};



export const getUserCall = ({ id, access_token }) => {
  
  return dispatch => {
    dispatch({ type: LOGIN_START });
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "ShowUserCommand",
   "id": id,
"destination service": "Users",
   "access_token": access_token }))
    .then(({ data }) => {
      if (!data.status) { 
       // console.log(data);
        return loginFail(dispatch, data.error);   
      }
      //console.log(data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.prof_pic);
      //console.log("so coool "+data.response.prof_pic+ " emaail "+ data.response.email);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };
};

export const editCall = ({id , access_token , firstName , lastName , email , password, age , country , profile_image}) => {
  return dispatch => {
    //dispatch({ type: EDIT });
    //find id and token
   console.log(age);
    if (firstName !== '') {
        return editName(dispatch, id, access_token, firstName);
    } else {
      if (lastName !== '') {
        return editLastName(dispatch, id, access_token, lastName);
      } else {
        if (email !== '') {
          return editEmail(dispatch, id, access_token, email);
        } else {
          if (password !== '') {
            return editPassword(dispatch, id, access_token, password);
          } else {
            if (age !== '') {
                return editAge(dispatch, id, access_token, age);
            } else{
              if (country !== '') {

              } else {
                if (profile_image !== '') {
                  return editProfile(dispatch, id, access_token, profile_image);
                }
              }
            }
          }
        }
      }
    }
  
  };
};


const editName = ( dispatch, id, access_token,firstName ) => {
    console.log(firstName+ "ya aan");
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "first_name": firstName }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);

        return loginFail(dispatch, data.error);   
      }
      //console.log(data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };

  const editLastName = ( dispatch, id, access_token,lastName ) => {
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "last_name": lastName }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);

        return loginFail(dispatch, data.error);   
      }
      //console.log(data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };

const editEmail = ( dispatch, id, access_token,email ) => {
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "email": email }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }
      console.log("edit email "+data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };


const editPassword = ( dispatch, id, access_token,password ) => {
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "pass": password }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };




const editProfile = ( dispatch, id, access_token,profile_pic ) => {
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "profile_pic": profile_pic.replace(URL_PIC,'') }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };

const editAge = ( dispatch, id, access_token,age ) => {
  console.log("age now "+ age);
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "UpdateUserCommand",
   "id": id,
    "destination service": "Users",
   "access_token": access_token,
   "age": age }))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);
        return loginFail(dispatch, data.error);   
      }
      console.log("age "+ data.response);
      localStorage.setItem(USER_ID, data.response.id);
      localStorage.setItem(ACCESS_TOKEN, data.response.access_token);
      localStorage.setItem(AGE, data.response.age);
      localStorage.setItem(COUNTRY, data.response.country);
      localStorage.setItem(EMAIL, data.response.email);
      localStorage.setItem(FIRST_NAME, data.response.first_name);
      localStorage.setItem(LAST_NAME, data.response.last_name);
      localStorage.setItem(PROFILE_PIC, data.response.profile_pic);
      localStorage.setItem(PASS, data.response.pass);
      //logInUser(dispatch, data);
      return dispatch({ type: SHOWUSER, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };


const loginFail = (dispatch, error) => {

   //console.log("111");
  dispatch({ type: LOGINFAIL, payload: error });
};

const logInUser = (dispatch, user) => {

  dispatch({ type: LOGIN, payload: user });
  //the user is contains only the access token and user_id
};

// const logInUser = (dispatch, user) => {

//   dispatch({ type: SHOWUSER, payload: user });
//   //the user is contains only the access token and user_id
// };




// abo@gmail.com
// 12345