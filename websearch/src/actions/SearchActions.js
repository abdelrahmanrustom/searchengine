import axios from 'axios';
import {
  SEARCH
} from './types';
import {
  BASE_URL,
  USER_ID
} from '../utils/data';


export const searchCall = ({ resultType, q, page_no, category }) => {
  console.log("resultType "+ resultType+ " q "+ q + "  page_no "+ page_no+ " category "+ category);
  return dispatch => {
    axios.post(BASE_URL, JSON.stringify({"ServiceCommand": "SearchCommand",
   "resultType": resultType,
   "q" :q,
   "page_no" : page_no,
   "category" : category,
   "user_id" : localStorage.getItem(USER_ID),
   "country" : "300",
   "destination service": "Search"}))
    .then(({ data }) => {
      if (!data.status) { 
        console.log(data);   
      }
      console.log(data.response);
      return dispatch({ type: SEARCH, payload: data.response });
    })
    .catch(error =>  console.log(error));
  };
};