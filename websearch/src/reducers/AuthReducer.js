
import { 
	LOGIN,
	LOGINFAIL,
	LOGIN_START,
	SHOWUSER
} from '../actions/types';

const INITIAL_STATE = {
	email: '', 
	password: '',
	user: null,
	error: '',
	loading: false
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case LOGIN:
			return { ...state, ...INITIAL_STATE, user: action.payload };
		case SHOWUSER:
			return { ...state, ...INITIAL_STATE, user: action.payload };

		case LOGINFAIL:
			return { ...state, error: action.payload, password: '', loading: false };
		case LOGIN_START:
			return { ...state, loading: true, error: '' };
		default:
			return state;
	}
};