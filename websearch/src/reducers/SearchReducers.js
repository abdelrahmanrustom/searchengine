import { 
	SEARCH
} from '../actions/types';


const INITIAL_STATE = {
	searches: []
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case SEARCH:
			return { ...state, ...INITIAL_STATE, searches: action.payload };
		default: return state;
	}
};