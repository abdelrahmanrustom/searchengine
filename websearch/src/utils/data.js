export const BASE_URL = 'http://localhost:8080';
export const USER_ID = 'id';
export const ACCESS_TOKEN = 'access_token';
export const AGE = 'age';
export const COUNTRY = 'country';
export const EMAIL = 'email';
export const FIRST_NAME = 'first_name';
export const LAST_NAME = 'last_name';
export const PASS = 'pass';
export const PROFILE_PIC = 'profile_pic';
export const URL_PIC = 'https://res.cloudinary.com/doodle-search/image/upload/';
//TODO hee goes the name of (key) of the shared preference
// export const BASE_URL = 'http://myblabber.com/be-master/api/sign-in';
// export const LOGIN_URL = `${BASE_URL}login`;