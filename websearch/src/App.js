import React, { Component } from 'react';
import logo from './logo.svg';
import { Home } from './components/home.js';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './RouterComponent';

class App extends Component {
  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
      <Router />
      </Provider>
    );
   
  }
}

 // <Home />
export default App;



 // <div className="App">
 //        <header className="App-header">
 //          <img src={logo} className="App-logo" alt="logo" />
 //          <h1 className="App-title">Welcome to React</h1>
 //        </header>
 //        <p className="App-intro">
 //          To get started, edit <code>src/App.js</code> and save to reload.
 //        </p>
 //      </div>