package common;

import admin.Configurable;
import com.rabbitmq.client.*;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;
import users.SQLDBConnectionPool;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;



//this the users server for the users
public class RpcServer implements Configurable {
    private ThreadPoolExecutor executorService;
    private ExecutorCompletionService<Delivery> executorCompletionService;

    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Connection connectionPublish;
    private Channel channel;
    private Channel channelPublish;

    private ConsumerWorker consumer;

    private PublisherWorker publisher;
    private Thread publisherThread;

    private String host;
    private static final String HOST_KEY = "host";
    private static final String DEFAULT_HOST = "localhost";

    private ClassManager classManager;
    private DependencyManager dependencyManager;

    private String queueName;

    private int errorReportingLevel;

    public RpcServer(Properties properties, String name) {
        Properties classMap = new Properties();
        Properties depMap = new Properties();
        this.queueName = name;
        try {
            InputStream is = getClass().getResourceAsStream("classmap.properties");
            InputStream is2 = getClass().getResourceAsStream("dependencies.properties");
            classMap.load(is);
            depMap.load(is2);
        } catch (IOException exception) {
            exception.printStackTrace();
            System.exit(0);
        }
        classManager = new ClassManager(classMap);
        dependencyManager = new DependencyManager(depMap, classManager);
        initProperties(properties);
        initConnectionFactory();
        initExecutorCompletionService();
    }

    private void initProperties(Properties properties) {
        host = properties.getProperty(HOST_KEY, DEFAULT_HOST);
    }

    private void initConnectionFactory() {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
    }

    private void initExecutorCompletionService() {
        executorService = new ThreadPoolExecutor(10, 10,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
        executorCompletionService = new ExecutorCompletionService<>(executorService);
    }

    public void start() throws IOException, TimeoutException {
        connection = connectionFactory.newConnection(Executors.newSingleThreadExecutor((runnable) ->
                new Thread(Thread.currentThread().getThreadGroup(), runnable,"Consumer Thread")));
        channel = connection.createChannel();

        connectionPublish = connectionFactory.newConnection(Executors.newSingleThreadExecutor());
        channelPublish = connectionPublish.createChannel();

        consumer = new ConsumerWorker();

        publisher = new PublisherWorker();
        publisherThread = new Thread(publisher, "Publisher Thread");

        consumer.start();
        publisherThread.start();
    }

    private final class ConsumerWorker {
        private DefaultConsumer consumer = new RpcConsumer(channel);

        public void start() {
            unfreeze();
        }

        void freeze() {
            try {
                channel.basicCancel(consumer.getConsumerTag());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        void unfreeze() {
            try {
                Map<String, Object> args = new HashMap<String, Object>();
                args.put("x-message-ttl", 1000);
                channel.queueDeclare(queueName, false, false, true, args);
                channel.basicConsume(queueName, true, consumer);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }



    private final class RpcConsumer extends DefaultConsumer {
        RpcConsumer(Channel channel) {
            super(channel);
        }

        private Delivery dispatchDelivery(Delivery delivery) {
            JSONObject jsonObject = new JSONObject(new String(delivery.getBody(), CharsetUtil.UTF_8));
            byte[] reply;
            try {
                final Command command = CommandParser.parseCommand(classManager, dependencyManager, jsonObject);
                reply = command.call().toString().getBytes();
            } catch(Exception e) {
                reply = new JSONObject().put("Error", e.getClass().toString()).toString().getBytes();
            }
            // System.out.println(new String(reply));
            return new Delivery(delivery.getEnvelope(), delivery.getProperties(), reply);
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            Future<Delivery> future = executorCompletionService.submit(() ->
                    dispatchDelivery(new Delivery(envelope, properties, body)));
        }
    }

    private void publishDelivery(Delivery delivery) throws IOException {
        AMQP.BasicProperties requestProperties = delivery.getProperties();
        String correlationId = requestProperties.getCorrelationId();
        String replyTo = requestProperties.getReplyTo();
        AMQP.BasicProperties.Builder replyPropertiesBuilder
                = new AMQP.BasicProperties.Builder().correlationId(correlationId);
        AMQP.BasicProperties replyProperties = replyPropertiesBuilder.build();
        channelPublish.basicPublish("", replyTo, replyProperties, delivery.getBody());
    }

    private final class PublisherWorker implements Runnable {
        private boolean isRunning = false;

        @Override
        public void run() {
            isRunning = true;
            while (isRunning) {
                try {
                    Delivery response = executorCompletionService.take().get();
                    publishDelivery(response);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }

        void freeze() {
            isRunning = false;
        }

        void unfreeze() {
            isRunning = true;
        }
    }

    @Override
    public void setMaxThreadCount(int maxThreadCount) {
        if(maxThreadCount < executorService.getCorePoolSize()) {
            executorService.setCorePoolSize(maxThreadCount);
            executorService.setMaximumPoolSize(maxThreadCount);
        } else {
            executorService.setMaximumPoolSize(maxThreadCount);
            executorService.setCorePoolSize(maxThreadCount);
        }
    }

    @Override
    public void setMaxDBConnectionCount(int maxDBConnectionCount) {
        try {
            ((SQLDBConnectionPool) dependencyManager.get(SQLDBConnectionPool.class))
                    .setMaxDBConnectionCount(maxDBConnectionCount);
        } catch (Exception exception) {
            exception.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void addCommand(String commandName) {
        updateClass(commandName);
    }

    @Override
    public void deleteCommand(String commandName) {
        classManager.deleteClass(commandName);
    }

    @Override
    public void updateCommand(String commandName) {
        updateClass(commandName);
    }

    @Override
    public void updateClass(String className) {
        classManager.updateClass(className);
    }

    @Override
    public void freeze() {
        publisher.freeze();
        consumer.freeze();
        executorService.shutdown();
        executorService = null;
        executorCompletionService = null;
        try {
            channel.close();
            connection.close();
            channel = null;
            connection = null;
            publisherThread = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unfreeze() {
        initExecutorCompletionService();
        initConnectionFactory();
        try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setErrorReportingLevel(int errorReportingLevel) {
        this.errorReportingLevel = errorReportingLevel;
    }
}
