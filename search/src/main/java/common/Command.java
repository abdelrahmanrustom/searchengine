package common;

import org.json.JSONObject;

import java.util.concurrent.Callable;

public abstract class Command implements Callable<JSONObject> {
    public abstract JSONObject call() throws Exception;
}
