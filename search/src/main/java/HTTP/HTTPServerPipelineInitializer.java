package HTTP;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpContentDecompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;

public class HTTPServerPipelineInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("HTTP Codec", new HttpServerCodec()); // In & Out

        pipeline.addLast("HTTP Content Decompressor", new HttpContentDecompressor()); // In
        pipeline.addLast("HTTP Content Compressor", new HttpContentCompressor()); // Out

        pipeline.addLast("HTTP Object Aggregator", new HttpObjectAggregator(65536)); // In
        pipeline.addLast("Chunked Write Handler", new ChunkedWriteHandler()); // Out

        pipeline.addLast("HTTP Request Handler", new HttpRequestHandler()); // In
        pipeline.addLast("HTTP Response Handler", new HttpResponseHandler()); // Out

        pipeline.addLast("JSON Request Decoder", new JsonRequestDecoder()); // In
        pipeline.addLast("JSON Response Encoder", new JsonResponseEncoder()); // Out

        pipeline.addLast("JSON Request Handler", new JsonRequestHandler()); // Application logic
    }
}
