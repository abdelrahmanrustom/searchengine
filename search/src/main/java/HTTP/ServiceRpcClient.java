package HTTP;

import com.rabbitmq.client.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class ServiceRpcClient {
    private Connection connection;
    private Channel channel;
    private String requestQueueName;
    private String replyQueueName;

    private final Map<String, Consumer<JSONObject>> _continuationMap = new HashMap<>();

    public ServiceRpcClient(String queueName) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        this.requestQueueName = queueName;

        connection = factory.newConnection();
        channel = connection.createChannel();
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("x-message-ttl", 1000);
        channel.queueDeclare(requestQueueName, false, false, true, args);
        replyQueueName = channel.queueDeclare("ReplyToHTTPServer", false, false, true, args).getQueue();
        // channel.basicQos(10);

        channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                Consumer<JSONObject> consumer;
                synchronized (_continuationMap) {
                    consumer = _continuationMap.remove(properties.getCorrelationId());
                }
                if(consumer != null) {
                    consumer.accept(new JSONObject(new String(body)));
                    System.out.println("client receving");
                }
            }
        });
    }

    public void call(String message, Consumer<JSONObject> callBack) throws IOException {
        byte[] messageBytes = message.getBytes("UTF-8");
        final String corrId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        try {
            synchronized (_continuationMap) {
                String replyId = "" + corrId;
                _continuationMap.put(replyId, callBack);
            }
            channel.basicPublish("", requestQueueName, props, messageBytes);
            System.out.println(message);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
