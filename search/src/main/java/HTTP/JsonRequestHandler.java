package HTTP;

import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.json.JSONObject;

public class JsonRequestHandler extends SimpleChannelInboundHandler<JSONObject> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, JSONObject msg) {
        Dispatcher.main.dispatchRequest(msg, (response) -> {
            ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
        });
    }
}
