package HTTP;

import org.json.JSONObject;

import java.util.function.Consumer;

public class Dispatcher {
    public static Dispatcher main = new Dispatcher();

    private ServiceRpcClient rpcClient;

    private Dispatcher() {
        try {
            rpcClient = new ServiceRpcClient("rpc_queue");
        } catch (Exception exception) {
            exception.printStackTrace();
            System.exit(0);
        }
    }

    public void dispatchRequest(JSONObject request, Consumer<JSONObject> consumer) {
        try {
            rpcClient.call(request.toString(), consumer);
        } catch (Exception exception) {
            System.out.println(exception.getLocalizedMessage());
            consumer.accept(new JSONObject().put("Error", exception.getLocalizedMessage()));
        }
    }
}
