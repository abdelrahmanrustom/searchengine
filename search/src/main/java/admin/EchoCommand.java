package admin;

import common.Command;
import org.json.JSONObject;

public class EchoCommand extends Command {
    private JSONObject params;

    public EchoCommand(JSONObject params) {
        this.params = params;
    }

    @Override
    public JSONObject call() {
        return params;
    }
}
