package admin;

public interface Configurable {
    void setMaxThreadCount(int maxThreadCount);
    void setMaxDBConnectionCount(int maxDBConnectionCount);
    void addCommand(String commandName);
    void deleteCommand(String commandName);
    void updateCommand(String commandName);
    void updateClass(String className);
    void freeze();
    void unfreeze();
    void setErrorReportingLevel(int errorReportingLevel);
}
