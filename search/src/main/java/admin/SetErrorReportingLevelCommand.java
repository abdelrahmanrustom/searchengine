package admin;

import common.Command;
import org.json.JSONObject;

public class SetErrorReportingLevelCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public SetErrorReportingLevelCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.setErrorReportingLevel(params.getInt("error reporting level"));
        return new JSONObject().put("Success", "true");
    }
}
