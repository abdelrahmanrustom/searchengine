package admin;

import common.Command;
import org.json.JSONObject;

public class UpdateCommandCommand extends Command {
    private JSONObject params;
    private Configurable receiver;

    public UpdateCommandCommand(Configurable server, JSONObject params) {
        this.params = params;
        this.receiver = server;
    }

    @Override
    public JSONObject call() {
        receiver.updateCommand(params.getString("command name"));
        return new JSONObject().put("Success", "true");
    }
}
