//package interests_history_nosql;
//
//import admin.AdminServer;
//import common.RpcServer;
//
//import java.io.IOException;
//import java.util.Properties;
//import java.util.logging.Logger;
//
//public class App {
//    private static final Logger logger = Logger.getLogger(AdminServer.class.getName());
//
//    private RpcServer rpcServer;
//    private AdminServer adminServer;
//
//    public App(Properties properties) {
//        this.rpcServer = new RpcServer(properties, properties.getProperty("name"));
//        this.adminServer = new AdminServer(this.rpcServer, properties);
//    }
//
//    public void start() {
//        try {
//            this.rpcServer.start();
//            this.adminServer.start();
//        } catch (Exception exception) {
//            logger.severe(exception.getLocalizedMessage());
//            System.exit(0);
//        }
//    }
//
//    public static void main(String[] args) {
//        Properties properties = new Properties();
//        try {
//            properties.load(users.App.class.getResourceAsStream("app.properties"));
//        } catch (IOException exception) {
//            logger.severe(exception.getLocalizedMessage());
//            System.exit(0);
//        }
//
//        users.App app = new users.App(properties);
//        app.start();
//    }
//}
