package rabbitmq;//import org.elasticsearch.client.*;
//import org.elasticsearch.*;

import org.apache.http.HttpHost;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

public class Elastic {

    public static void main(String[] args) throws IOException {
        final RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http"),
                        new HttpHost("localhost", 9201, "http")));

        CreateIndexRequest request = new CreateIndexRequest("webpages");
        request.settings(Settings.builder()
                .put("index.number_of_shards", 5)
                .put("index.number_of_replicas", 2)
        );

        request.mapping("webpage",
                "  {\n" +
                        "    \"webpage\": {\n" +
                        "      \"properties\": {\n" +
                        "        \"plain_text\": {\n" +
                        "          \"type\": \"text\"\n" +
                        "        },\n" +
                        "        \"url\": {\n" +
                        "          \"type\": \"text\"\n" +
                        "        },\n" +
                        "        \"description\": {\n" +
                        "          \"type\": \"text\"\n" +
                        "        },\n" +
                        "        \"pagerank\": {\n" +
                        "          \"type\": \"float\"\n" +
                        "        },\n" +
                        "        \"title\": {\n" +
                        "          \"type\": \"text\"\n" +
                        "        },\n" +
                        "        \"category\": {\n" +
                        "          \"type\": \"integer\"\n" +
                        "        },\n" +
                        "        \"lastModified\": {\n" +
                        "          \"type\": \"date\"\n" +
                        "        },\n" +
                        "        \"keywords\": {\n" +
                        "          \"type\": \"keyword\"\n" +
                        "        },\n" +
                        "        \"db_id\": {\n" +
                        "          \"type\": \"keyword\"\n" +
                        "        },\n" +
                        "        \"imgs\": {\n" +
                        "          \"type\": \"keyword\"\n" +
                        "        },\n" +
                        "        \"vids\": {\n" +
                        "          \"type\": \"keyword\"\n" +
                        "        },\n" +
                        "        \"has_imgs\": {\n" +
                        "          \"type\": \"boolean\"\n" +
                        "        },\n" +
                        "        \"has_vids\": {\n" +
                        "          \"type\": \"boolean\"\n" +
                        "        },\n" +
                        "        \"suggest\": {\n" +
                        "          \"type\": \"completion\"\n" +
                        "        }\n" +
                        "      }\n" +
                        "    }\n" +
                        "  }",
                XContentType.JSON);
//        request.alias(
//                new Alias("web")
//        );

        /*
         Timeout to wait for the all the nodes to acknowledge the index creation as a TimeValue
         Timeout to wait for the all the nodes to acknowledge the index creation as a String
         */

//        request.timeout(TimeValue.timeValueMinutes(2));
//        request.timeout("2m");
//
//        /*
//        Timeout to connect to the master node as a TimeValue
//        Timeout to connect to the master node as a String
//         */
//        request.masterNodeTimeout(TimeValue.timeValueMinutes(1));
//        request.masterNodeTimeout("1m");

        /*
         The number of active shard copies to wait for before the create index API returns a response, as an int.
         The number of active shard copies to wait for before the create index API returns a response, as an ActiveShardCount.
//         */
//        request.waitForActiveShards(2);
//        request.waitForActiveShards(ActiveShardCount.DEFAULT);

        // sync
//        CreateIndexResponse createIndexResponse = client.indices().create(request);

//        CompletableFuture<Boolean> cf = new CompletableFuture<Boolean>();
        client.indices().createAsync(request, new ActionListener<CreateIndexResponse>() {
            public void onResponse(CreateIndexResponse createIndexResponse) {
                boolean acknowledged = createIndexResponse.isAcknowledged();
                boolean shardsAcknowledged = createIndexResponse.isShardsAcknowledged();

                System.out.println(acknowledged + " " + shardsAcknowledged);
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
