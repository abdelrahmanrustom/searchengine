package rabbitmq;

import com.rabbitmq.client.*;
import common.Constants;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class RabbitMqClient {
    private Connection connection;
    private Channel channel;
    private int appSize;

    private Map<String, String> replyQueueName = new HashMap<>();
    private String rmqid;

    private final Map<String, Consumer<byte[]>> _continuationMap = new HashMap<String, Consumer<byte[]>>();

    public RabbitMqClient(String rmqid) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        this.rmqid = rmqid;

        connection = factory.newConnection();
        channel = connection.createChannel();

        appSize = Constants.applications.length;
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("x-message-ttl", 1000);
        for (int i = 0; i < appSize ; i++){
            channel.queueDeclare(Constants.applications[i] + "", false, false, true, args);
            // channel.basicQos(10);

            String replyQueue = channel.queueDeclare(
                    "Reply-To-RabbitMQ-" + rmqid + "-" + Constants.applications[i],
                    false,
                    false,
                    true,
                    args
            ).getQueue();

            replyQueueName.put(Constants.applications[i] + "", replyQueue);

            channel.basicConsume(replyQueue, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    Consumer<byte[]> consumer;
                    synchronized (_continuationMap) {
                        consumer = _continuationMap.remove(properties.getCorrelationId());
                    }
                    if(consumer != null) {
                        consumer.accept(body);
                        System.out.println("client receving");
                    }
                }
            });
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                for(String queue : replyQueueName.keySet()) {
                    channel.queueDelete(queue);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }

    public void call(byte[] message, Consumer<byte[]> callBack) {
        JSONObject jsonObject = new JSONObject(new String(message));
        String appName = jsonObject.getString("destination service");

        final String corrId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName.get(appName))
                .build();

        try {
            synchronized (_continuationMap) {
                String replyId = "" + corrId;
                _continuationMap.put(replyId, callBack);
            }
            channel.basicPublish("", appName, props, message);
            System.out.println(new String(message));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
