package loadbalancer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class LoadBalancerServer {

    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Channel channel;

    private ConsumerWorker consumer;

    private String host;
    private static final String HOST_KEY = "host";
    private static final String DEFAULT_HOST = "localhost";
    private static  String RPC_QUEUE_NAME = "rpc_queue";

    LoadBalancerClient rabbitMqClient = new LoadBalancerClient(2);

    public LoadBalancerServer() throws IOException, TimeoutException {
        initConnectionFactory();
        start();
    }


    private void initConnectionFactory() {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
    }

    public static void main(String... args) throws Exception {
        LoadBalancerServer b = new LoadBalancerServer();
    }


    public void start() throws IOException, TimeoutException {
        connection = connectionFactory.newConnection(Executors.newSingleThreadExecutor((runnable) ->
                new Thread(Thread.currentThread().getThreadGroup(), runnable,"Consumer Thread")));


        channel = connection.createChannel();
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("x-message-ttl", 1000);
        channel.queueDeclare(RPC_QUEUE_NAME, false, false, true, args);
        // channel.basicQos(10);

        consumer = new ConsumerWorker();

        consumer.start();

    }

    private final class ConsumerWorker {
        private DefaultConsumer consumer = new CommandRpcConsumer(channel);

        public void start() {
            try {
                // System.out.println(Thread.currentThread());
                channel.basicConsume(RPC_QUEUE_NAME, true, consumer);
            } catch (IOException exception) {
                exception.printStackTrace();
                try {
                    this.shutdown();
                } catch (Exception shutdownException) {
                    shutdownException.printStackTrace();
                }
            }
        }

        void shutdown() {
            try {
                channel.basicCancel(consumer.getConsumerTag());
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    private final class CommandRpcConsumer extends DefaultConsumer {
        CommandRpcConsumer(Channel channel) {
            super(channel);
        }

        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
            rabbitMqClient.call(body, (byte[] replyBody) -> {
                String correlationId = properties.getCorrelationId();
                String replyTo = properties.getReplyTo();
                AMQP.BasicProperties.Builder replyPropertiesBuilder
                        = new AMQP.BasicProperties.Builder().correlationId(correlationId);
                AMQP.BasicProperties replyProperties = replyPropertiesBuilder.build();
                try {
                    System.out.println("Server sending");
                    channel.basicPublish("", replyTo, replyProperties, replyBody);
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            });
        }
    }

}
