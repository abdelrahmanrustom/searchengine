package loadbalancer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class LoadBalancerClient {
    private Connection connection;
    private Channel channel;


    private ArrayList<String> clientArrayList;
    private ArrayList<String> replyArrayList;

    private final Map<String, Consumer<byte[]>> _continuationMap = new HashMap<String, Consumer<byte[]>>();

    public LoadBalancerClient(int size) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        connection = factory.newConnection();
        channel = connection.createChannel();
        clientArrayList = new ArrayList<>();
        replyArrayList = new ArrayList<>();
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("x-message-ttl", 1000);
        RoundRobin.maxClients = size;
        for (int i = 0; i < size; i++) {
            channel.queueDeclare(i + "", false, false, true, args);
            // channel.basicQos(10);
            clientArrayList.add(i + "");
            String replyQueue = channel.queueDeclare(
                    "Reply-To-LoadBalancer-From-" + i,
                    false,
                    false,
                    true,
                    args).getQueue();
            replyArrayList.add(replyQueue);

            channel.basicConsume(replyQueue, true, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                    Consumer<byte[]> consumer;
                    synchronized (_continuationMap) {
                        consumer = _continuationMap.remove(properties.getCorrelationId());
                    }
                    if(consumer != null) {
                        consumer.accept(body);
                        System.out.println("client receving");
                    }
                }
            });
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                for(String queue : replyArrayList) {
                    channel.queueDelete(queue);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }

    public void call(byte[] message, Consumer<byte[]> callBack) {
        try {
            AMQP.BasicProperties props;
            String client;
            synchronized (_continuationMap) {
                client = clientArrayList.get(RoundRobin.currentQueueName);
                RoundRobin.currentQueueName = RoundRobin.currentQueueName + 1;
                if (RoundRobin.currentQueueName == RoundRobin.maxClients) {
                    RoundRobin.currentQueueName = 0;
                }
                final String corrId = UUID.randomUUID().toString();

                props = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(corrId)
                        .replyTo(replyArrayList.get(RoundRobin.currentQueueName))
                        .build();
                String replyId = "" + corrId;
                _continuationMap.put(replyId, callBack);
            }
            channel.basicPublish("", client, props, message);
            System.out.println(new String(message));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
