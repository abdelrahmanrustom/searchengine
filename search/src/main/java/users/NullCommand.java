package users;

import common.Command;
import org.json.JSONObject;

public class NullCommand extends Command {
    @Override
    public JSONObject call() {
        return new JSONObject();
    }
}
