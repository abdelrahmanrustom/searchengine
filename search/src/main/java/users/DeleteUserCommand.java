package users;

import common.Command;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.concurrent.ExecutionException;

public class DeleteUserCommand extends Command {


    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;

    public DeleteUserCommand(SQLDBConnectionPool pool, ReddisCluster cluster, JSONObject params) {
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = cluster;
    }

    @Override
    public JSONObject call() throws ExecutionException, InterruptedException {
        JSONObject response;

        try {
            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString("delete");
                        if (!tmp2.equals("")){
                            tmp.put("id", tmp2);
                            Integer value =(Integer) this.params.get("id");
                            String users  = reddisCluster.get(value.toString());
                            if (users != null) {

                                reddisCluster.delete(value.toString());
                            }
                            return new JSONObject()
                                    .put("response",tmp)
                                    .put("error", "")
                                    .put("status", "true");
                        } else {
                            return new JSONObject()
                                    .put("response","")
                                    .put("error", "Auth error")
                                    .put("status", "false");
                        }

                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "User Not Found!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.getClass())
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", "User Not Found!")
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            response =  new JSONObject()
                    .put("response", "")
                    .put("error", e.getClass())
                    .put("status", "false");
        }

        return response;
    }

    private String getQueryString() {
        MessageFormat mf = new MessageFormat("SELECT delete({0}, ''{1}'');");
        Object[] parsedParams = new Object[] {
                params.get("id"), params.get("access_token")};


        return mf.format(parsedParams);
    }
}
