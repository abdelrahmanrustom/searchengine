package users;

import com.lambdaworks.redis.RedisClusterAsyncConnection;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.cluster.RedisClusterClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReddisCluster {
    static RedisClusterClient rcc;
    static {
        List<RedisURI> initialUris = new ArrayList<RedisURI>();
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7000).build());
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7001).build());
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7002).build());
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7003).build());
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7004).build());
        initialUris.add(RedisURI.Builder.redis("127.0.0.1", 7005).build());
        rcc = RedisClusterClient.create(initialUris);
    }

    public void set(String key, String value){
        RedisClusterAsyncConnection<String, String> connection = rcc.connectClusterAsync();
        connection.set(key, value);
    }


    public String get(String key) throws ExecutionException, InterruptedException {
        RedisClusterAsyncConnection<String, String> connection = rcc.connectClusterAsync();
        return connection.get(key).get();
    }

    public void delete(String key){
        RedisClusterAsyncConnection<String, String> connection = rcc.connectClusterAsync();
        connection.del(key);
    }


}
