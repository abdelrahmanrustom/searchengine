package users;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.function.Function;

public class SQLDBConnectionPool {
    private static HikariDataSource hikariDataSource;

    static {
        Properties props = new Properties();
        props.setProperty("dataSourceClassName", "org.postgresql.ds.PGSimpleDataSource");
        props.setProperty("dataSource.user", "postgres");
        props.setProperty("dataSource.password", "1234");
        props.setProperty("dataSource.databaseName", "mydb");
        props.put("dataSource.logWriter", new PrintWriter(System.out));

        HikariConfig config = new HikariConfig(props);
        hikariDataSource = new HikariDataSource(config);
    }

    public JSONObject executeQuery(String query, Function<ResultSet, JSONObject> parser) throws SQLException {
        try(Connection connection = hikariDataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(query);
            ResultSet resultSet = stmt.executeQuery();
            JSONObject result = parser.apply(resultSet);
            return result;
        }
    }

    public void setMaxDBConnectionCount(int n) {
        hikariDataSource.setMaximumPoolSize(n);
    }
}