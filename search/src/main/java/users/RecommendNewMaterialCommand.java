package users;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import common.Command;
import org.apache.http.HttpHost;
import org.bson.Document;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;

import static com.mongodb.client.model.Filters.eq;

public class RecommendNewMaterialCommand  extends Command {
    int user_id = -1;
    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;
    final RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http"),
                    new HttpHost("localhost", 9201, "http")));

    private static final MongoClient mongoClient = new
            MongoClient(new MongoClientURI("mongodb://localhost:27017"));
    private  static  final MongoDatabase database = mongoClient.getDatabase("history");
    private static final MongoCollection<Document> collection = database.getCollection("interests");
    private static final MongoCollection<Document> collection1 = database.getCollection("history");
    Random random = new Random();
    private Integer devReportLvl;

    public void parseParams(int user_id){
        this.user_id = user_id;
    }


    public RecommendNewMaterialCommand(SQLDBConnectionPool pool, ReddisCluster cluster,JSONObject params){
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = cluster;
        //this.params = params;
    }

    public JSONObject call(){

        try {
            parseParams(params.getInt("user_id"));
        }catch (Exception e){
            JSONObject res = new JSONObject()
                    .put("response", "")
                    .put("error", "Bad request")
                    .put("status", "false");
            return res;
        }

        String keywords = "";
        int interest;

        Document d1;
        FindIterable<Document> d2;
        try {
            d1 = collection.find(eq("user_id", user_id)).first();
            int[] _interests = (int[]) d1.get("interests");
            interest = _interests[random.nextInt(4)];
            d2 = collection1.find(eq("user_id", user_id));
            int i = 0;
            for(Document d: d2){
                keywords += d.getString("query") + " ";
                if(++i > 5)
                    break;
            }

        }catch (Exception e){
            String err = devReportLvl == 1 ? e.toString() : "Database failure!";
            JSONObject response = new JSONObject()
                    .put("response", "")
                    .put("error", err)
                    .put("status", "false");
            return  response;
        }

        SearchRequest searchRequest = new SearchRequest("webpages");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder tmp = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("category",
                        interest))
                .should(QueryBuilders.matchQuery("keywords", keywords).fuzziness(Fuzziness.AUTO));

        FunctionScoreQueryBuilder query = QueryBuilders.functionScoreQuery(tmp,
                ScoreFunctionBuilders.fieldValueFactorFunction("pagerank"))
                .boostMode(CombineFunction.SUM);
        searchSourceBuilder.query(query);
        searchRequest.source(searchSourceBuilder);

        try {
            SearchHits hits =  client.search(searchRequest).getHits();
            ArrayList<String> json_search_responses = new ArrayList<>();
            for(SearchHit hit: hits){
                json_search_responses.add(hit.getSourceAsString());
            }
//                System.out.println(json_search_responses);
            JSONObject response = new JSONObject()
                    .put("response", json_search_responses)
                    .put("error", "")
                    .put("status", "true");
            return response;
        } catch (IOException e) {
            String err = devReportLvl == 1 ? e.toString() : "Server failure!";
            JSONObject response = new JSONObject()
                    .put("response", "")
                    .put("error", err)
                    .put("status", "false");
            return  response;
        }
    }
}
