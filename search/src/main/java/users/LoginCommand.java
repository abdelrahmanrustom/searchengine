package users;

import common.Command;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.MessageFormat;

public class LoginCommand extends Command {
    private SQLDBConnectionPool receiver;
    private JSONObject params;
    private ReddisCluster reddisCluster;

    public LoginCommand(SQLDBConnectionPool pool, ReddisCluster cluster, JSONObject params) {
        this.receiver = pool;
        this.params = params;
        this.reddisCluster = cluster;
    }

    @Override
    public JSONObject call() {
        JSONObject response = null;
        System.out.print(reddisCluster);

        try {


//
           // Integer value =(Integer) this.params.get("id");

            response = receiver.executeQuery(getQueryString(), (params) -> {
                try {
                    if (params.next()) {
                        JSONObject tmp = new JSONObject();
                        String tmp2 = params.getString("login");
                        tmp2 = tmp2.substring(1, tmp2.length()-1);
                        String[] tmps = tmp2.split(",");
                        tmp.put("id", tmps[0]);

                        String users  = reddisCluster.get(tmps[0]);
                        if (users != null) {

                            reddisCluster.delete(tmps[0]);
                        }
                        tmp.put("access_token", tmps[1]);

                        return new JSONObject()
                                .put("response", tmp)
                                .put("error", "")
                                .put("status", "true");
                    } else {
                        return new JSONObject()
                                .put("response", "")
                                .put("error", "User Not Found!")
                                .put("status", "false");
                    }
                } catch (SQLException exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", exception.getClass())
                            .put("status", "false");
                } catch (Exception exception) {
                    return new JSONObject()
                            .put("response", "")
                            .put("error", "User Not Found!")
                            .put("status", "false");
                }
            });
        } catch (SQLException e) {
            response =  new JSONObject()
                    .put("response", "")
                    .put("error", e.getClass())
                    .put("status", "false");
        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }

        return response;
    }

    private String getQueryString() {
        MessageFormat mf = new MessageFormat("SELECT login(''{0}'', ''{1}'');");
        Object[] parsedParams = new Object[] {
                params.get("email"), params.get("pass")};

        return mf.format(parsedParams);
    }
}
