
CREATE OR REPLACE FUNCTION delete(id1 INTEGER)
RETURNS void AS $$
BEGIN
   DELETE FROM USERS
    WHERE _id = id1;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION login(
    email1 VARCHAR(50),
    pass1 VARCHAR(50),
    OUT id_out   INTEGER,
    OUT token_out   TEXT)
AS $$
    DECLARE 
    e1 VARCHAR(50) := '';
    p1 VARCHAR(50) := '';
    _id1 INTEGER   := 0 ;
    t  TEXT := '';
BEGIN
    SELECT _id, email, pass into _id1, e1, p1
    FROM Users
    WHERE pass1 = pass AND email1 = email;
    
        
    IF (e1 = '') IS NOT FALSE THEN
        t = 'Wrong username or password';
    ELSE
        t = CAST(_id1 AS TEXT) || md5(random()::text);
        id_out = _id1;
        token_out = t;
    UPDATE USERS
        SET access_token = t
        WHERE
        _id = _id1;
    END IF;
END; $$ 
LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION signup(
    first_name1 VARCHAR(50),
    last_name1 VARCHAR(50),
    email1 VARCHAR(50),
    pass1 VARCHAR(50),
    country1 INTEGER,
    age1 INTEGER DEFAULT NULL,
    profile_pic1 VARCHAR(50) DEFAULT NULL,
    OUT id_out   INTEGER,
    OUT token_out   TEXT)
AS $$

BEGIN
  INSERT INTO Users(first_name, last_name, email, pass, age, country, profile_pic)
  VALUES (first_name1, last_name1, email1, pass1, age1, country1, profile_pic1);
  
  SELECT _id, access_token into id_out, token_out
    FROM Users
    WHERE pass1 = pass AND email1 = email;
    
END; $$
LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION update_user_firstname(
    id1 INTEGER, 
    access_token1 TEXT,
    first_name1 VARCHAR(50)
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET first_name = first_name1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION update_user_lastname(
    id1 INTEGER, 
    access_token1 TEXT,
    last_name1 VARCHAR(50)
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET last_name = last_name1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION update_user_email(
    id1 INTEGER, 
    access_token1 TEXT,
    email1 VARCHAR(50)
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET email = email1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION update_user_pass(
    id1 INTEGER, 
    access_token1 TEXT,
    pass1 VARCHAR(50)
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET pass = pass1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION update_user_country(
    id1 INTEGER, 
    access_token1 TEXT,
    country1 INTEGER
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET country = country1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION update_user_age(
    id1 INTEGER, 
    access_token1 TEXT,
    age1 INTEGER
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET age = age1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION update_user_profile_pic(
    id1 INTEGER, 
    access_token1 TEXT,
    profile_pic1 VARCHAR(50)
    )
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    UPDATE USERS 
    SET profile_pic = profile_pic1
    WHERE
    _id = id1 AND access_token = access_token1
    RETURNING * into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION authorize_token(
    id INTEGER,  access_token1 TEXT, OUT id_out INTEGER)
AS $$
BEGIN
    SELECT _id into id_out
    FROM Users
    WHERE _id = id  AND access_token = access_token1;
END; $$ 
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION show(id1 INTEGER, token TEXT)
RETURNS Users AS $$
DECLARE 
  ret RECORD;
BEGIN
    Select * 
    From USERS
    WHERE
    _id = id1 AND token = access_token
    into ret;
    return ret;
END; $$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION logout(id1 INTEGER, token text,  OUT id_out INTEGER)
AS $$
BEGIN
    UPDATE USERS
    SET access_token = ''
    WHERE
    _id = id1 AND token = access_token
    RETURNING _id into id_out;
END; $$
LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION delete(id1 INTEGER, token text, OUT id_out INTEGER)
AS $$
BEGIN
   DELETE FROM USERS
    WHERE _id = id1 AND token = access_token
    RETURNING _id into id_out;
END; $$
LANGUAGE PLPGSQL;